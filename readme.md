## LzyScheduler by LzyFocus

LzyScheduler is currently a web application designed to help people complete tasks and meet deadlines by scheduling and prioritizing the things they have to do.

It generates a priority listing of the best tasks to focus on based on urgency (based on due date), importance, time required for completion and through the use of a fixed priority preemptive scheduling algorithm.

This scheduling algorithm is commonly used in real time systems by way of executing task with the highest priority of all the task that are currently ready for execution.  The pre-emptive scheduler has a clock interrupt task that can provide the user with options to switch after the task has had a given period to execute—the time slice. This scheduling system has the advantage of making sure no task hogs the user’s time for any time longer than the time slice, which in this instance is dependent on the users attention span. However, this scheduling scheme is vulnerable to some amount of lagging as number of task increases: since priority is given to higher-priority tasks, the lower-priority tasks could wait an indefinite amount of time. One common method of arbitrating this situation is aging, which gradually increments the priority of waiting tasks, ensuring that they will all eventually execute before the given deadline.

To overcome the inherent limitation of the algorithm, the urgency portion of the priority score is increased as the due date approaches.


The aim is to have LzyScheduler available on all the major mobile platforms.

## Official Documentation

Documentation for the application will soon be posted on the official [LzyScheduler website](http://lzyscheduler.fsi-tech.com).

### Contributing To LzyScheduler

**All issues and pull requests should be filed on the [vgr2/lzyscheduler](https://bitbucket.org/vgr2/lzyscheduler) repository.**

### License

All rights reserved
