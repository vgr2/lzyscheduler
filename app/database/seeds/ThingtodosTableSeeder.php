<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;

class ThingtodosTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Thingtodo::create([
				'title' => $faker->word,
				'description' => $faker->paragraph,
				'progress' => $faker->numberBetween(1,100),
				'given' => $faker->dateTime($max = 'now'),
				'due' => $faker->dateTimeBetween('+3 days', '+30 days'),
				'allottedtime' => $faker->numberBetween(0,100),
				'allday' => $faker->boolean($chanceOfGettingTrue = 50),
				'actualstart' => $faker->dateTime($max = 'now'),
				'actualend' => $faker->dateTimeBetween('+3 days', '+30 days'),
				'importance' => $faker->numberBetween(0,10),
				'urgency' => $faker->numberBetween(0,100),
				'priority' => $faker->numberBetween(0,100),
				'relativeposition' => $faker->numberBetween(0,10),
				'recur' => $faker->boolean($chanceOfGettingTrue = 50),
				'user_id' => $faker->numberBetween(1,4)
			]);
		}
	}

}
