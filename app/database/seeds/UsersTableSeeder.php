<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->delete();

		$faker = Faker::create();

		foreach(range(1, 1) as $index)
		{
			User::create(
				array(
					'firstname' => 'Val',
					'lastname' => 'Rowe',
					'password' => Hash::make('val'),
					'email' => 'val@lzyfocus.net'
				)
			);
			User::create(
				array(
					'firstname' => 'Jon',
					'lastname' => 'Thompson',
					'password' => Hash::make('jon'),
					'email' => 'jon@lzyfocus.net'
				)
			);
			User::create(
				array(
					'firstname' => 'Anthoney',
					'lastname' => 'White',
					'password' => Hash::make('anthoney'),
					'email' => 'anthoney@lzyfocus.net'
				)
			);
			User::create(
				array(
					'firstname' => 'Darrick',
					'lastname' => 'Foster',
					'password' => Hash::make('darrick'),
					'email' => 'darrick@lzyfocus.net'
				)
			);
		}
	}

}
