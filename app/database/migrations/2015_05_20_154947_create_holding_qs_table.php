<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHoldingQsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('holding_qs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('recipient');
			$table->string('subject');
			$table->text('message')->nullable();
			$table->datetime('sendtime');
			$table->integer('user_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('holding_qs');
	}

}
