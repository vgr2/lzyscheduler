<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddAlldayToThingtodosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('thingtodos', function(Blueprint $table)
		{
			$table->boolean('allday')->default(false)->after('allottedtime');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('thingtodos', function(Blueprint $table)
		{
			$table->dropColumn('allday');
		});
	}

}
