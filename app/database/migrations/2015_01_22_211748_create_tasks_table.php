<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTasksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tasks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->text('description')->nullable();
			$table->datetime('due')->nullable();
			$table->decimal('allottedtime')->nullable();
			$table->integer('importance')->default(5);
			$table->decimal('progress')->default(1);
			$table->decimal(' urgency')->nullable();
			$table->decimal('priority')->nullable();
			$table->decimal('relativeposition')->nullable();
			$table->boolean('recur')->nullable();
			$table->integer('user_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	
	public function down()
	{
		Schema::drop('tasks');
	}

}
