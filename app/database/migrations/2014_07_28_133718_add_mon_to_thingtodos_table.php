<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddMonToThingtodosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('thingtodos', function(Blueprint $table)
		{
			$table->boolean('mon')->nullable()->after('sun');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('thingtodos', function(Blueprint $table)
		{
			$table->dropColumn('mon');
		});
	}

}
