<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddEndsToThingtodosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('thingtodos', function(Blueprint $table)
		{
			$table->string('ends')->nullable()->after('startson');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('thingtodos', function(Blueprint $table)
		{
			$table->dropColumn('ends');
		});
	}

}
