<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateThingtodosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('thingtodos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->text('description')->nullable();
			$table->decimal('progress');
			$table->datetime('given');
			$table->datetime('due');
			$table->decimal('allottedtime');
			$table->datetime('actualstart')->nullable();
			$table->datetime('actualend')->nullable();
			$table->integer('importance')->default(5);
			$table->decimal('urgency')->nullable();
			$table->decimal('priority')->nullable();
			$table->decimal('relativeposition')->nullable();
			$table->boolean('recur')->nullable();
			$table->integer('user_id')->unsigned();
//			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('thingtodos');
	}

}
