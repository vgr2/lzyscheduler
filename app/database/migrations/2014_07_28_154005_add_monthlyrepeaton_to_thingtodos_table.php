<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddMonthlyrepeatonToThingtodosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('thingtodos', function(Blueprint $table)
		{
			$table->string('monthlyrepeaton')->nullable()->after('sat');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('thingtodos', function(Blueprint $table)
		{
			$table->dropColumn('monthlyrepeaton');
		});
	}

}
