<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFriToThingtodosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('thingtodos', function(Blueprint $table)
		{
			$table->boolean('fri')->nullable()->after('thu');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('thingtodos', function(Blueprint $table)
		{
			$table->dropColumn('fri');
		});
	}

}
