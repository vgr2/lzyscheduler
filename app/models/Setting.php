<?php

class Setting extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'timezone' => 'required',
		'user_id' => 'required|integer',
	];

	// Don't forget to fill this array
	protected $fillable = ['timezone', 'user_id'];

    public function user() {
		return $this->belongsTo('User');
	}
    
    public function setting() {
		return $this->hasMany('Priority');
	}
    
}