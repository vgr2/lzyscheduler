<?php

class Task extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
        'title' => 'required',
        'importance' => 'required|numeric|min:1|max:10',
        'due' => 'required|date',
        'allottedtime' => 'required|numeric',
        'progress' => 'required|numeric|min:0|max:1',
        'user_id' => 'required',
	];

	// Don't forget to fill this array
	protected $fillable = ['title', 'description', 'due', 'allottedtime','importance','progress', 'urgency','priority', 'relativeposition','recur','user_id'];

        

}
