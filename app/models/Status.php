<?php

class Status extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'task_id' => 'required|integer',
		'status' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['task_id', 'status'];

}