<?php

class HoldingQ extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
        'recipient' => 'required|email',
        'subject' => 'required',
        'message' => 'required',
        'sendtime' => 'required',
        'user_id' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['recipient', 'subject', 'message', 'sendtime', 'user_id'];

}