<?php

class Thingtodo extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'title' => 'required',
		'description' => '',
		'importance' => 'required|numeric|min:1|max:10',
//		'given' => 'required|date',
		'due' => 'required|date',
		'allottedtime' => 'required|numeric',
		'progress' => 'required|min:0|max:100|numeric'

	];

	protected $table = 'thingtodos';

	// Don't forget to fill this array
	protected $fillable = ['title', 'location', 'description', 'importance', 'given', 'due', 'allottedtime', 'progress', 'user_id'];
    
	public function user() {
	 return $this->belongsTo('User');
	}

}