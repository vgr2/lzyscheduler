<?php 
/**
* Send a reminder -time & title- of the dday's engagements to each user
*/
class DailyReminders
{
	var $sendQueue, $timeRange, $events;
	function __construct()
	{
		# code...
		// get events starting on current day
		$this->timeRange = getTimeRange();
		$events = $this->getEvents($this->timeRange);
		$processedEvents = $this->processUsers($events);
		
		// if numQueueItems > 100                                                                                             
	}

    public function getTimeRange() {
        return $timeRange = array(
            'start' => date('Y-m-d').' 00:00:00', 
            'end' =>  date('Y-m-d').' 23:59:59' 
            );
    }

    public function getEvents($timeRange){
        // $userId = Confide::user()->id;
        $users = User::with('thingtodos')
			->where($timeRange['start'], '<=', 'given' ) // given time is after the start of day
			->where($timeRange['end'], '>=', 'given' ) // given time is after the start of day
	        ->where('progress', '<', '1')  // incomplete
	        ->whereRaw('allottedtime = (TIMESTAMPDIFF(minute,given,due)/60)')
	        ->get();        
        $events = DB::table('thingtodos')
	        ->where($timeRange['start'], '<=', 'given' ) // given time is after the start of day
	        ->where($timeRange['end'], '>=', 'given' ) // given time is after the start of day
	        // ->where('progress', '>=', '1')  // incomplete
	        ->whereRaw('allottedtime = (TIMESTAMPDIFF(minute,given,due)/60)')
	        ->orderBy('given','ASC')
	        ->get();        
        return $users;        
    }

    public function processUsers($users) {
    	foreach ($users as $user) {
            Mail::queue('emails.notify.daily', $data, function($message) use ($user){
                $message->to($user->email, $user->firstname.' '.$user->lastname)
                        ->subject('Your things to do today');
            });
    	}
		


    }

    public function sendMessage($user_id)
    {
    	$user = User::find($user_id);
    	return $user->email;
    }
}
?>