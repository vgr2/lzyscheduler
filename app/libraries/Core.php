<?php
/**
 * Description of Core
 *
 * @author Valdeck
 */
class Core {
  protected $db, $timezone, $config, $priority;
  public function __construct() {
    $this->config = array (
    /* tz config */
    'timezone' => 'America/Jamaica',
    
    /* db config */
    'dbuser' => 'root',
    'dbpassword' => '',
    'dbname' => 'lzyfocus',
    'dbhost' => 'localhost',
    
    /* scale used to calc percentages, eg. 5 out of scale 10 = 50%, therefore 1 - 10 */
    'importanceScale' => 10,
    'progressScale' => 100,
    
    /* number of priority sections for visually grouping tasks */
    'numPrioritySections' => 4,
    
    /* extra time: increase the time needed for completion by given percentage */
    'extraTime' => true,
    'extraTimeFactor' => 1.1,
    );

/* priority section names NB. must # correspond to value of $numPrioritySections */
    $this->priority[] = array(
        'id' => '1',
        'name' => 'Most Urgent / Most Important',
        'rangeMin' => 0.75, // for priority
        'rangeMax' => 1, // for priority
        'urgencyRangeMin' => 0.75,
        'urgencyRangeMax' => 1,
        'importanceRangeMin' => 0.75,
        'importanceRangeMax' => 1,
        
    );
    $this->priority[] = array(
        'id' => '2',
        'name' => 'More Urgent / Less Important',
        'rangeMin' => 0.5, // for priority
        'rangeMax' => 0.75, // for priority
        'urgencyRangeMin' => 0.5,
        'urgencyRangeMax' => 0.75,
        'importanceRangeMin' => 0.5,
        'importanceRangeMax' => 0.75,
    );
    $this->priority[] = array(
        'id' => '3',
        'name' => 'Less Urgent / More Important',
        'rangeMin' => 0.25, // for priority
        'rangeMax' => 0.5, // for priority
        'urgencyRangeMin' => 0.25,
        'urgencyRangeMax' => 0.5,
        'importanceRangeMin' => 0.25,
        'importanceRangeMax' => 0.5,
    );
    $this->priority[] = array(
        'id' => '4',
        'name' => 'Least Urgent / Least Important',
        'rangeMin' => 0, // for priority
        'rangeMax' => 0.25, // for priority
        'urgencyRangeMin' => 0,
        'urgencyRangeMax' => 0.25,
        'importanceRangeMin' => 0,
        'importanceRangeMax' => 0.25,
    );


//    $this->db = new ezSQL_mysql($this->config['dbuser'], $this->config['dbpassword'], $this->config['dbname'], $this->config['dbhost']);
    $this->timezone = $this->config['timezone'];
    setlocale(LC_ALL, $this->config['timezone']);

    

  }

  public function dd($param) {
    echo "<pre>";
    print_r($param);
    echo "</pre>";
    
  }
  
  
}

?>
