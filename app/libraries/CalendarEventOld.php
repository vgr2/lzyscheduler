<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Manipulate event source data for display in the calendar
 * @author Valdeck
 */
class CalendarEventOld extends Core {
  var $timezone;
  public function __construct() {
        Core::__construct();    
//        dd($this->timezone);
  }
  /**
   * Accepts an events object and returns a formatted string for calendar input
   * @param object $events
   * @return string
   */
  public function formatEventsForFullCalendar($events) {
    $i = 1;
    $count = count($events);
    $rv = "";
    foreach ($events as $event) {
        $rv .= '{ ';
        $rv .= "id: '".$event->id."', ";
        $rv .= "title: '".$event->title." @ ".$event->location."', ";
        $rv .= "url: '".URL::route('edit', $event->id)."', ";
        $rv .= "start: '".$event->given."', ";
        $rv .= "end: '".$event->due."' ";
        if ($event->allday == "1"){
          $rv .= ",";
          $rv .= "allDay: 'true'";
        }
        $rv .= "}";
        $rv .= ($i  < $count ? "," : "");
        $i++;
    }
    return $rv;
  }
  public function formatEventsForJQICalendar($events) {
    $i = 1;
    $count = count($events);
    $rv = "";
    foreach ($events as $event) {
        $rv .= '{ ';
        $rv .= "uid: '".$event->id."', ";
        $rv .= "title: '".$event->title." @ ".$event->location."', ";
        $rv .= "url: '".URL::route('edit', $event->id)."', ";
        $rv .= "begins: '".$event->given."', ";
        $rv .= "ends: '".$event->due."', ";
        $rv .= "}";
        $rv .= ($i  < $count ? "," : "");
        $i++;
    }
    return $rv;
  }
  
  /**
   * return an array of TTD whose alloted time is < diff between task end time and star ttime.
   * @param type $userId
   * @param type $thingsToDo
   */
  public function getTasks($thingsToDo) {
//    var_dump($thingsToDo);
    $rv = array();
//    foreach ($thingsToDo as $ttd) {
//      echo '<br>'.$ttd->urgency;
//    }
//    foreach ($thingsToDo as $thing) {
//      if(($this->timeAvail($this->timeObj($thing->given), $this->timeObj($thing->due))) > ($thing->allottedtime)){
//        $rv[] = $thing;
//      }
//    }
    return $rv;
  }

  public function timeObj($time) {
    return new DateTime($time, new DateTimeZone($this->timezone));
  }
  /**
   * 
   * @param integer $userId
   * @param object $thingsToDo
   * @return object with all the event type things to do belonging to this user
   */
  public function getEvents($thingsToDo){
    
    $rv = array();
    foreach ($thingsToDo as $thing) {
      if(($this->timeAvail($thing->given, $thing->due)) == ($thing->allottedtime)){
        $rv[] = $thing;
      }
    }
    return $rv;
  }
  
    public function isTask($allottedTime, $timeAvail) {
//        if ($allottedTime < $timeAvail){
        if ($allottedTime != $timeAvail){
          return TRUE;
        }
    }
    /**
     * 
     * @param type $allottedTime
     * @param type $timeAvail
     * @return boolean
     */
    public function isEvent($allottedTime, $timeAvail) {
        if ($allottedTime == $timeAvail){
          return TRUE;
        }
    }


  
  /**
   * Returns the difference between two datetimes in hours
   * @param datetime string $currentTime
   * @param datetime string $timeDue
   * @return type $hours
   */
  public function timeAvail($currentTime, $timeDue){
    $diff = $this->timeObj($currentTime)->diff($this->timeObj($timeDue));
    $hrs = $diff->h+($diff->days*24)+($diff->i/60);
    $hours = round(doubleval($diff->format("%R$hrs")),2);
    return $hours;
  }
  public function compareDateTimes($dt1, $dt2) {
    return strtotime($dt1['given']) > strtotime($dt2['given']);
  }

  public function startDateSort($events) {
    usort($events, 'compareDateTimes');
    return $events;
  }

  public function getFreePeriods($events) {
    $sortedEvents = startDateSort($events);
    $freePeriods = [];
    for ($i = 0; $i < count($sortedEvents)-1; $i++) {
      $freePeriod = new ArrayObject();
      $freePeriod['starttime'] = $sortedEvents[$i]['due'];
      $freePeriod['endtime'] = $sortedEvents[$i+1]['given'];
      $freePeriods[] = $freePeriod;
    } 
    return $freePeriods;
  }
public function dd($param) {
    echo "<pre>";
    print_r($param);
    echo "</pre>";
}
  

}

?>
