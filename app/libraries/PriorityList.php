<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PriorityList
 *
 * @author Valdeck
 */
class PriorityList extends PriorityGenerator{
  
  public function __construct($tasks,$completed=null) {
    $tasks = PriorityGenerator::__construct($tasks);
    $this->sortprioritys($tasks);
//    array_reverse($this->tasks);
//    $this->db->vardump($this->tasks);
//    $this->listTasksByPriority($this->priorityGroups, $this->tasks);
  }
  
//  public function listByPriority($tasks,$config) {
//    foreach ($tasks as $task) {
//      $pg[] = new PriorityList($task,$config);
//    }
//  }
  
  public function compareprioritys($arr1,$arr2) {
    return strnatcmp($arr2->priority, $arr1->priority); 
  }

  public function sortprioritys($tasks) { 
    usort($tasks, array($this, 'compareprioritys')); 
  }

  public function priorityGroups(){
    $priorityGrps = $this->priority;
    return $priorityGrps;
  }
  
  public function listTasksByPriority($todos) {    
    $priorityGrps = $this->priorityGroups;
    $p = 0; $t = 0;
    $rv = ""; // = '<div class="taskContainer">';
    foreach ($priorityGrps as $priorityGrp) {
    	$p++;
      $rv .= '<div class="groupHeader">';
      $rv .= $p.' '.$priorityGrp['name'];
      $rv .= '</div>';
      $rv .= '<div class="groupTasks">';
      $rv .= '<ul>';
      foreach ($todos as $todo) {
        if ($todo['priorityGrpId'] == $priorityGrp['id']){
        	$t++;
		  	$rv .= '<li>';
			    $rv .= $p.' '.'<strong><a href="'.URL::route('edit', $todo->id).'">'.$todo['title'].'</a></strong> ';
			    $rv .= '<br>Progress: '.($todo['progress']*100).'% ';
			    $rv .= ' Due on: '.$todo['due'].'. ';
			    $rv .= ' Priority Score: '. $todo['priority'];
			$rv .= '</li>';
        }
      }
      $rv .= '</ul>';
      $rv .= '</div>';
    }
    // $rv .= '</div>';
    return $rv;
  }

  public function listPriorityGroups($priorityGrps, $todos)
  {
  	$rv = "";
  	foreach ($priorityGrps as $priorityGrp) {
  		$rv .= $this->priorityGrpItem($priorityGrp, $todos);
  	}
  	return $rv;
  }

  public function priorityGrpItem($priorityGrp, $todos)
  {
    if (!isset($todos)) {
      $rv = '';
    } else {
//    	 $rv = '<div class="row table-bordered">';
         $rv = '<div class="col-lg-6 col-xs-12">';
          $rv .= '<div class="panel panel-default">';
            $rv .= '<div class="panel-heading">';
               $rv .= '<div class="panel-title">';
            		$rv .= $priorityGrp['name'];
              $rv .= '</div>'; // end pt
            $rv .= '</div>'; // end ph
          	$rv .= '<div class="panel-body">';
          	  // $rv .= '<ul>';
            		$rv .= $this->listPriorityItems($todos, $priorityGrp['id']);
          	  // $rv .= '</ul>';
          	$rv .= '</div>'; //end pb
           $rv .= '</div>'; // end p
         $rv .= '</div>'; // end cols
//       $rv .= '</div>'; //end row
    }
	return $rv;
  }

  public function listPriorityItems($todos, $priorityGrpId)
  {
    usort($todos, function ($a, $b) { // added to sort the internal values in each priority group
                    return $a->relativeposition - $b->relativeposition;
                });

  	$rv = "";
	foreach ($todos as $todo) {
		$rv .= $this->priorityListItem($todo, $priorityGrpId);
	}
  	return $rv;
  }

  public function priorityListItem($todo, $priorityGrpId)
  {
//    echo '<pre>';print_r($todo->priorityGrpId);die();
    $pgId = $todo->priorityGrpId;
  	$rv = "";
  	if ($pgId == $priorityGrpId){
        $url = 'http://www.google.com/calendar/event?action=TEMPLATE';
        $url .= '&text='.$todo->title;
        $url .= '&dates='.date('Ymd\THis').'/'.date('Ymd\THis');
        $url .= '&details='.$todo->description;
        $url .= '&location=';
        $url .= '&ctz='.$this->config['timezone'];
//        $url .= '&output=xml';
        // $url .= '&trp=false';
        // $url .= '&sprop=';
        // $url .= '&sprop=name:';

        $dtl = 'Due on: '.$todo->due.'; ';
        $dtl .= 'Progress: '.($todo->progress*100).'%; ';
        $dtl .= ' Priority Score: '. $todo->priority;

        $rv .= '<li>';
		    $rv .= '<strong><a href="'.URL::route('edit', $todo->id).'" title="'.$dtl.'">'.$todo->title.'</a></strong> ';
            $rv .= '<a href="'.$url.'" class=" glyphicon glyphicon-calendar" target="_blank" rel="nofollow" title="Schedule work time"></a> ';
         $rv .= ' <strong>Due on:</strong> '.$todo->due.'. ';
         $rv .= ' <strong>Progress:</strong> '.($todo->progress*100).'% ';
         $rv .= ' Priority Score: '. $todo->priority;

		$rv .= '</li>';
	}
	return $rv;
  }

  public function getAllTags($todos)
  {
    $str = '';
    foreach ($todos as $t) {
      $str = $str . ' '.$t->title;
    }
    preg_match_all('|\[(.*?)\]|', $str, $regs) ;
    $tags = $regs[1];
    return $tags; // return tags
    
  }

  public function getTasksFromTag($todos, $tag){
    foreach ($todos as $t) {
      if ($tag == $this->getStringBetween($t->title, '[', ']')){
        $tasks[] = $t;
      }
    }
    return $tasks;
  }

  public function getStringBetween($string, $start, $end){
    $string = " ".$string;
    $ini = strpos($string,$start);
    if ($ini == 0) return "";
    $ini += strlen($start);
    $len = strpos($string,$end,$ini) - $ini;
    return substr($string,$ini,$len);
  }
 
}

?>
