<?php 

/**
* Accept tasks and generate urgency & priority scores for each one
*/
class Prioritize
{
	public $tasks, $timezone;
	
	public function __construct($tasks, $timezone)
	{
		
		$this->timezone = $timezone;
		// print_r($this->timezone);die();
		$this->tasks = $this->setPriorityScores($tasks);

		return $this->tasks;

	}

	public function setPriorityScores($todos) {    
		// print_r($todos);
		foreach ($todos as $todo) {
	        $pScore = $this->priorityScore(
	            $uScore = $this->urgencyScore(
					$todo->timeavail = $this->timeAvail($this->currentTime(), $this->timeObj($todo->due)), 
					$todo->timeneeded = $this->timeNeeded($todo->allottedtime, $todo->progress)
					), 
	            $this->importanceScore(
					$todo->importance
					)
			);
			$todo->urgency   = round(doubleval($uScore), 2);
			$todo->priority  = round(doubleval($pScore), 2);
			if ($todo->timeavail < 0){
				$todo->overdue = true;
			}        
		}
		return $todos;
	}

	/**
	* Calculate the priority score or the given task based on its urgency and importance scores.
	* @return double
	*/
	public function priorityScore($urgencyScore, $importanceScore) {
		return ($urgencyScore + $importanceScore)/2;
	}

	/**
	* calculate the urgency score of the given task
	* @return int
	*/
	public function urgencyScore($timeAvail,$timeNeeded) {

		if (($timeAvail >= $timeNeeded) && ($timeAvail > 0) ){
			$val = $timeNeeded / $timeAvail;
		} else {
			$val = 1;
		}
		return $val;
	}

	/**
	* Calculate importance score of a given task based on the importance scale and the given importance rating
	* @return double
	*/
	public function importanceScore($importance) {
		return $importance;
	}

	/**
	* Returns the current time for the set time zone
	* @return \DateTime
	*/
	public function currentTime() {
		return new DateTime("now", new DateTimeZone($this->timezone));
	}

	public function timeDue() {
		return new DateTime($this->timedue, new DateTimeZone($this->timezone));;
	}
	public function timeObj($time) {
		return new DateTime($time, new DateTimeZone($this->timezone));
	}

	/**
	* Calculate the time available (difference) in hours between the current time and time due
	* @param DateTime $currentTime Current time based on timezone settings
	* @param DateTime $timeDue Deadline time of task based on timezone settings
	* @return double
	*/
	public function timeAvail($currentTime, $timeDue){

		$diff = $currentTime->diff($timeDue);
		$hrs = $diff->h+($diff->days*24)+($diff->i/60);
		$hours = $diff->format("%R$hrs");

		return round(doubleval($hours),2);
	}

	/**
	* Calculate the time needed in hours based on the etc and current progress + 10%
	* @return double
	*/
	public function timeNeeded($etc,$progress) {
		return $timeNeeded = $etc - ($progress * $etc);
	}

}

?>