<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Notify - detect when notifications are 
 * The system sends users a message at certain time before of a deadline or a start time for some thing to do, 
 * As well as the system should give a morning notification stating everything that on the priority list 
 * Event 
 * Check all the events within a datetime range and if the check time is between 
 * reminder time and the start time send the reminder and mark as sent
 * Daily
 * get all the tasks for the day for each user and send notification
 * events: 24 hr, 15min and on time
 * tasks: 
 * @author Valdeck
 */
class Notify extends CalendarEvent{
//  
//  action items
//    get time range
//    get events in time range
//    for each event
//      get reminder time
//      get event time (start/end depending)
//      if current time >= reminder time and < event time
//        send notification
//        mark as sent
//      endif
//    endfor
//  
//  knowledge items
//    time range
//      start time
//      end time
//    events
//    reminder time
//    event time 
//    current time
//    notification message
//    user contact info
//      email
//      11 digit phone #
//    reminder status
//    
//  
    
    public function getTimeRange() {
        
    }
    public function getEvents($startTime, $endTime){
        
    }
    /**
     * process each event
     */
    public function processUsers($events) {
        
    }
    public function getReminderTime($event) {
        
    }
    public function getEventTime($event) {
        
    }
    public function approveSend($reminderTime, $eventTime) {
        
    }
    public function getMessage($event) {
        
    }
    public function sendMessage($event) {
        
    }

}

?>
