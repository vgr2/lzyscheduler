<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PriorityGenerator
 *
 * @author Valdeck
 */
class PriorityGenerator extends Core{
  public 
  //         $title, //string, task title/name
  //         $importance, //int, on a scale of 1 - 10
  //         $timedue, //datetime, the time by which the task is to be completed
  //         $etc, //int, amount of time in hours needed to complete the task
  //         $progress, //double, percentage of the progress made toward completion
  //         $task, //array, storing values for the above variables
  //         $priorityScore, //decimal, non relative priority rating
          $tasks//, //array, storing values for each task
  //         $priorityGroups, //array, storing priority
  //         $positionalRank
          ;

  public function __construct ($tasks,$completed=null) {
    Core::__construct();
    $this->cfg = $this->config;    
    
    $this->priorityGroups = $this->priority;
//    $this->db->vardump($priority);
    $this->tasks = $this->setPriorityScores($tasks);
    
    $this->tasks = $this->positionalRanking($this->tasks);
    
    if ($completed == true) {
      $this->tasks = $this->completeTasks($this->tasks);
    } 
//    else {
//      $this->tasks = $this->incompleteTasks($this->tasks);
//    }
//    $this->relativePosnStats($tasks);
    $scoresByGroup = $this->scoresByGroup($this->tasks, $this->priorityGroups);
//    die();
//    echo '<pre>';print_r($this->tasks); die();
//    $this->db->vardump($this->tasks);
//    $this->db->vardump($this->groupTasksByPriority($this->priorityGroups, $this->tasks));
    return $this->tasks = $this->placeTasksByPriority($scoresByGroup, $this->tasks);
//    return $this->tasks = $this->groupTasksByPriority($this->priorityGroups, $this->tasks);
  }
  
  public function setPriorityScores($todos) {    
//    var_dump($todos);
    foreach ($todos as $todo) {
      // assign pri score
        
//      $pScore = $this->priorityScore(
//              $uScore = $this->urgencyScore(
//                      $todo['timeavail'] = $this->timeAvail($this->currentTime(), $this->timeObj($todo['due'])), 
//                      $this->timeNeeded($todo['allottedtime'], $todo['progress'])
//                      ), 
//              $this->importanceScore(
//                      $todo['importance']
//                      )
//              );
//      $todo['urgency']   = round(doubleval($uScore), 2);
//      $todo['priority']  = round(doubleval($pScore), 2);
//      if ($todo['timeavail'] < 0){
//        $todo['overdue'] = 'yes';
//      }

            $pScore = $this->priorityScore(
                $uScore = $this->urgencyScore(
                      $todo->timeavail = $this->timeAvail($this->currentTime(), $this->timeObj($todo->due)), 
                      $this->timeNeeded($todo->allottedtime, $todo->progress)
                      ), 
                $this->importanceScore(
                      $todo->importance
                      )
              );
      $todo->urgency   = round(doubleval($uScore), 2);
      $todo->priority  = round(doubleval($pScore), 2);
      if ($todo->timeavail < 0){
        $todo->overdue = 'yes';
      }        
    }
    return $todos;
  }
  
  /**
   * Generate a list of the highest priority tasks of a 4 group priority list
   * @param array $todos
   * @return array
   */
  public function groupTasksByPriority($priorityGrps, $todos) {
    //$newTodo = [];
    foreach ($priorityGrps as $priorityGrp) {
      foreach ($todos as $todo) {
        if (($todo['priority'] > $priorityGrp['rangeMin']) && ($todo['priority'] <= $priorityGrp['rangeMax']) && $todo['progress'] < 1){
//        if (($todo['relativeposition'] > $priorityGrp['rangeMin']) && ($todo['relativeposition'] <= $priorityGrp['rangeMax']) && $todo['progress'] < 0){
          $todo['priorityGrpId'] = $priorityGrp['id'];
          $newTodo[] = $todo;
        }
      }
    }
    return $newTodo;
  }
  
  public function groupByRelativePosition($priorityGrps, $tasks) {
    $numGrps = count($priorityGrps);
  }
   
  /**
   * Returns the current time for the set time zone
   * @return \DateTime
   */
  public function currentTime() {
    return new DateTime("now", new DateTimeZone($this->timezone));
  }
  
  public function timeDue() {
    return new DateTime($this->timedue, new DateTimeZone($this->timezone));;
  }
  public function timeObj($time) {
    return new DateTime($time, new DateTimeZone($this->timezone));
  }
  
  /**
   * Calculate the time available (difference) in hours between the current time and time due
   * @param DateTime $currentTime Current time based on timezone settings
   * @param DateTime $timeDue Deadline time of task based on timezone settings
   * @return double
   */
  public function timeAvail($currentTime, $timeDue){
//    $currentTime = $this->timeObj($currentTime);
//    $timeDue = $this->timeObj($timeDue);
    $diff = $currentTime->diff($timeDue);
    $hrs = $diff->h+($diff->days*24)+($diff->i/60);
    $hours = $diff->format("%R$hrs");
    return round(doubleval($hours),2);
  }
  
  /**
   * Calculate the time needed in hours based on the etc and current progress + 10%
   * @return double
   */
  public function timeNeeded($etc,$progress) {
    $timeNeeded = $etc - ($progress * $etc);
    if ($this->cfg['extraTime']){
      $timeNeeded = $timeNeeded * $this->cfg['extraTimeFactor'];
    }
    return $timeNeeded;
  }
  
  /**
   * calculate the urgency score of the given task
   * @return int
   */
  public function urgencyScore($timeAvail,$timeNeeded) {
//    $timeAvail = $this->timeAvail($currentTime, $timeDue);
//    $timeNeeded = $this->timeNeeded();
   if (($timeAvail >= $timeNeeded) && ($timeAvail > 0) ){
    // if (($timeAvail < 0) || ($timeAvail > 0) ){
      /* @var $val int */
      $val = $timeNeeded / $timeAvail;

    } else {
      $val = 1;
    }
    return $val;
  }
  
  /**
   * Calculate importance score of a given task based on the importance scale and the given importance rating
   * @return double
   */
  public function importanceScore($importance) {
    return $importance / $this->config['importanceScale'];
  }
  /**
   * Calculate the priority score or the given task based on its urgency and importance scores.
   * @return double
   */
  public function priorityScore($urgencyScore, $importanceScore) {
    return ($urgencyScore + $importanceScore)/2;
  }

  /**
   * calculate the positional values of the priority scores to determine the grouping and position
   * on a %age scale for grouping and placement
   * @return array
   */
  public function positionalRanking($tasks) {
    $totalPScore = 0;
    foreach ($tasks as $task) {
//      $totalPScore += $task['priority'];
      $totalPScore += $task->priority;
    }
//    dd($totalPScore);
    foreach ($tasks as $task) {
//      $task['relativeposition'] = round(doubleval($task['priority'] / $totalPScore),2)*100 ;
      $task->relativeposition = round(doubleval($task->priority / $totalPScore),2)*100 ;
    }
    return $tasks;
  }
  public function scoresByGroup($tasks,$priorities) {
//    print_r($tasks);die()
    $relpos = [];
    $scores = [];
    foreach ($tasks as $t) {
     $relpos[] = $t->relativeposition;
    }
//    echo '<pre>';
//    print_r($relpos);
//    die();
    rsort($relpos);
    sort($priorities);
    $numGroups = count($priorities);
    $grouped = $this->partition($relpos, $numGroups);
//    foreach ($priorities as $pkey => $pvalue) {
////      echo '<br>'.($pvalue['name']);
      for ($i = 0; $i < count($grouped); $i++) {
//        echo '<br>Group: '.$i;
  //      print_r($grouped[$i]);
        for ($j = 0; $j < count($grouped[$i]); $j++) {
//          echo '<br>--score: '.$grouped[$i][$j];//['name'] = $pvalue['name'];
          for ($k = 0; $k < count($grouped[$i][$j]); $k++) {
            foreach ($priorities as $pkey => $pvalue) {
  //            echo '<br>--pkey: '.$pkey;
  //            echo '<br>--Group: '.$i;
              if($i == $pkey){
                $score = $grouped[$i][$j];
                $gId = $priorities[$i]['id'];
                $gName = $priorities[$i]['name'];
                
                $scores[] = array('score' => $score, 'groupId' => $gId, 'groupName' => $gName);
              }

            }
          }
        }
      }
//    }
      
//    echo '<br>';
//    print_r($scores);

    return $scores;
  }
  
//  ****** write 2 funcs with incomplete & complete tasks respectively
  public function completeTasks($todos) {
    $newTodo = [];
    foreach ($todos as $todo) {
//        echo '<pre>';print_r($todo);die();
      if ($todo->progress >= 1){
        $newTodo[] = $todo;
      }
    }
    return $newTodo;
  }
  
  public function incompleteTasks($todos) {
//    $newTodo = [];
    foreach ($todos as $todo) {
//        echo '<pre>';print_r($todo);die();
      if (($todo['progress'] < 1) && ($this->timeAvail($this->timeObj($todo->given), $this->timeObj($todo->due)) > 0)){
        $newTodo[] = $todo;
      }
    }
    return $newTodo;
  }
  
  public function placeTasksByPriority($relevantScores, $todos) {
    $newTodo = [];
    foreach ($relevantScores as $relScore) {
//      print_r($relScore);
      foreach ($todos as $todo) {
//        echo '<pre>';print_r($todo);die();
        if ($todo->relativeposition == $relScore['score']){
//        if (($todo['relativeposition'] > $priorityGrp['rangeMin']) && ($todo['relativeposition'] <= $priorityGrp['rangeMax']) && $todo['progress'] < 0){
          $todo->priorityGrpId = $relScore['groupId'];
          $newTodo[] = $todo;
        }
      }
    }
    return $newTodo;
  }
  
  public function partition( $list, $p ) {
    $listlen = count( $list );
    $partlen = floor( $listlen / $p );
    $partrem = $listlen % $p;
    $partition = array();
    $mark = 0;
    for ($px = 0; $px < $p; $px++) {
        $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
        $partition[$px] = array_slice( $list, $mark, $incr );
        $mark += $incr;
    }
    return $partition;
}
//  list.sort();
//n = list.length;
//part1 = list[0 : n/3];
//part2 = list[n/3 : 2*n/3];
//part3 = list[2*n/3 : n];
  public function relativePosnStats($tasks) {
    $ttl = 0;
    echo '<style type="text/css">';
    echo 'th {padding:6px;}';
    echo 'td {padding:6px;}';
    echo '</style>';
    echo '<table border="1" cellpadding="2">';
    echo "<tr><th>[Title]</th><th> [Allotted time] </th><th> [Progress] </th><th> [Time Needed] </th><th> [Time Avail] </th><th> [Urgency] </th><th> [Priority] </th><th> [Relative Posn] </th></tr>";
    foreach ($tasks as $task) {
      echo "<tr><td>".$task['title']."</td><td>".$task['allottedtime']."</td><td>".$task['progress']."</td><td>".$this->timeNeeded($task['allottedtime'], $task['progress'])."</td><td>".$task['timeavail']."</td><td>".$task['urgency']."</td><td>".$task['priority']."</td><td>".$task['relativeposition']."</td></tr>";
      $ttl += $task['relativeposition'];
    }
    echo '</table>';
    echo "<br>Total: $ttl";
  }
}

?>
