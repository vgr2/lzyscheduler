<?php

Route::get('/', array('as' => 'home', 'uses' => 'HomeController@getHome'));
Route::get('/events', array('as' => 'events', 'uses' => 'HomeController@getIndex'))->before('auth');
Route::get('/day', array('as' => 'day', 'uses' => 'HomeController@getDay'))->before('auth');
Route::get('/week', array('as' => 'week', 'uses' => 'HomeController@getWeek'))->before('auth');
Route::get('/month', array('as' => 'month', 'uses' => 'HomeController@getMonth'))->before('auth');
Route::get('/year', array('as' => 'year', 'uses' => 'HomeController@getYear'))->before('auth');

Route::get('/gcal', array('as' => 'gcal', 'uses' => 'HomeController@googleCal'))->before('auth');

Route::get('/login', array('as' => 'login', 'uses' => 'AuthController@getLogin'))->before('guest');
Route::post('login', array('uses' => 'AuthController@postLogin'))->before('csrf');

Route::get('/logout', array('as'=>'logout', 'uses' => 'AuthController@logOut'))->before('auth');


//Route::get('/new', array('as'=>'new', 'uses' => 'ThingtodosController@create'))->before('auth');
//Route::post('/new', array('uses' => 'ThingtodosController@store'))->before('csrf');

//Route::get('/edit/{thing}', array('as'=>'edit', 'uses' => 'ThingtodosController@edit'))->before('auth');
//Route::patch('/edit', array('uses' => 'ThingtodosController@update'))->before('csrf');

Route::get('/prioritylist', array('as'=>'prioritylist', 'uses' => 'HomeController@priorityGroupNew'))->before('auth');
Route::get('/completedlist', array('as'=>'completedlist', 'uses' => 'HomeController@completePriorityItemsNew'))->before('auth');

Route::resource('thingtodos', 'ThingtodosController');
Route::resource('home', 'HomeController');
Route::resource('settings', 'SettingsController');
Route::resource('priorities', 'PrioritiesController');
Route::resource('tasks', 'TasksController');
Route::resource('holdingq', 'HoldingQsController');
Route::resource('statuses', 'StatusesController');

Route::get('/tasks/create', array('as'=>'/tasks/create', 'uses' => 'TasksController@create'))->before('auth');
Route::post('/tasks/create', array('uses' => 'TasksController@store'))->before('csrf');

Route::get('/edit/{thing}', array('as'=>'edit', 'uses' => 'TasksController@edit'))->before('auth');
Route::post('/tasks/{task}', array('uses' => 'TasksController@update'))->before('csrf');

// route for email reminders
Route::post('/emailreminder', array('as' => 'emailreminder', 'uses' => 'HomeController@sendReminderEmail' ));

// route for calendar reminders
Route::post('/downloadcal', ['as' => 'downloadcal', 'uses' => 'HomeController@generateICS'])->before('csrf');

// Confide routes
Route::get('users/create', 'UsersController@create');
Route::post('users', 'UsersController@store');
Route::get('users/login', 'UsersController@login')->before('guest');
Route::post('users/login', 'UsersController@doLogin');
Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');
Route::get('users/logout', 'UsersController@logout');

// Dashboard route
Route::get('userpanel/dashboard', function(){ return View::make('userpanel.dashboard'); });
 
// Applies auth filter to the routes within admin/
Route::when('userpanel/*', 'auth');

Route::get('qdaily', ['as' => 'qdaily', 'uses' => 'EmailReminderController@dailyReminder']);


Route::get('bullseye', array('as'=>'bullseye', 'uses' => 'HomeController@bullseye'))->before('auth');
Route::get('bullseyelist', array('as'=>'bullseyelist', 'uses' => 'HomeController@bullseyeTable'))->before('auth');
Route::get('update_status', array('as' => 'update_status', 'uses' => 'HomeController@updateStatus' ))->before('auth');
Route::get('settings', array('as' => 'mysettings', 'uses' => 'SettingsController@showSetting'))->before('auth');
Route::post('update_tz', array('as' => 'update_tz', 'uses' => 'SettingsController@update_tz'))->before('auth');