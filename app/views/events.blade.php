<?php $page = "Events"; ?>
@extends('layouts.main-bootstrap-fixed')

@section('head')
    @parent

	{{-- Fullcalendar JavaScript and CSS--}}
	<link rel='stylesheet' href="{{ URL::asset('fullcalendar/fullcalendar.css') }}"/>
	<script src="{{ URL::asset('fullcalendar/lib/jquery.min.js') }}"></script>
	<script src="{{ URL::asset('fullcalendar/lib/moment.min.js') }}"></script>
	<script src="{{ URL::asset('fullcalendar/fullcalendar.js') }}"></script>

        {{-- Initialize Fullcalendar --}}
	<script type="text/javascript">
		$(document).ready(function() {

		    $('#calendar').fullCalendar({
		        // put your options and callbacks here
		        
		        header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay'
				},
				defaultDate: '{{ date("Y-m-d") }}',
				defaultView: 'agendaDay',
				editable: true,
				events: [
				<?php  
                $cal = new CalendarEvent();
                echo $calevents = $cal->formatEventsForFullCalendar($thingtodos);
				?>
				]
		    });

		});
	</script>
@stop

@section('content')
	<div class="h2">Welcome</div>
	<p>Lzy Scheduler helps you by taking all the things you have to do, organizing and prioritizing them, into a daily, weekly or monthly schedule.</p>
    
   
    <!-- <h2>Today</h2> -->
	{{-- summary of todays events 
	- showing overall start and end time. 
	- comma separated list of events
	--}}
	<!-- <h2>Now</h2> -->
    
    <!-- <h2>Next Event</h2> -->
    
    <!-- <h2>Calendar</h2> -->
	<div id="calendar"></div>
	
	

	<!--<pre>{{ print_r($thingtodos) }}</pre>-->


	<pre>{{-- eventFormat($thingtodos); --}}</pre>
{{--
	<h2>Things To Do</h2>
	<ul>		
		@foreach ($thingtodos as $thingtodo)
			<li>
			{{ Form::open() }}

				{{ Form::text('title', $thingtodo->title, array('class' => '')) }}
				Progress: {{ Form::text('progress', $thingtodo->progress, array('data-slider' => 'true')) }}
				Due: {{ Form::text('due', $thingtodo->due )}}
				Importance {{ Form::text('importance', $thingtodo->importance, array('data-slider' => 'true') ) }}
				{{ Form::text('timeestimate', $thingtodo->timeestimate ) }}
				{{ Form::radio('timeunit', $thingtodo->timeunit, $thingtodo->timeunit == 'hours' ? true : '' ) }}hours
				{{ Form::radio('timeunit', $thingtodo->timeunit, $thingtodo->timeunit == 'minutes' ? true : '' ) }}minutes

			{{ Form::close() }}
			</li>
		@endforeach
	</ul>
	--}}

@stop