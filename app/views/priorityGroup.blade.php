<?php $page = "Priority List"; ?>
@extends('layouts.main-bootstrap-fixed')

@section('head')
    @parent
	<!-- <link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" /> -->
	{{ HTML::style('jquery-ui-1.11.0.custom/jquery-ui.css', array('media' => 'all')) }}
    {{ HTML::style('timepicker/jquery-ui-timepicker-addon.css') }}
    
    {{ HTML::script('jquery-ui-1.11.0.custom/jquery-ui.js') }}
	<!-- <script src="{{ URL::asset('datetimepicker/jquery.js') }}"></script> -->
	{{ HTML::script('timepicker/jquery-ui-timepicker-addon.js') }}
    {{ HTML::style('datetimepicker/jquery.datetimepicker.css') }}
    {{ HTML::script('datetimepicker/jquery.datetimepicker.js') }}

    <style type="text/css">
      .taskContainer{
        border: 1px solid antiquewhite;
        font-family: Gill, Helvetica, sans-serif;
      }
      .groupHeader{
        font-weight: bold;
        font-size: 14px;
      }
      .groupTasks{margin: 2px;}
      .groupTasks p{margin: 2px;}
      .clsDatePicker {z-index: 100000;}
    </style>
	<script type="text/javascript">
	 // $(document).ready(function(){
	// 	var startDateTextBox = $('#given');
	// 	var endDateTextBox = $('#due');

	// 	startDateTextBox.datetimepicker({ 
	// 		timeFormat: 'HH:mm:00',
	// 		dateFormat: 'yy-mm-dd',
	// 		onClose: function(dateText, inst) {
	// 			if (endDateTextBox.val() != '') {
	// 				var testStartDate = startDateTextBox.datetimepicker('getDate');
	// 				var testEndDate = endDateTextBox.datetimepicker('getDate');
	// 				if (testStartDate > testEndDate)
	// 					endDateTextBox.datetimepicker('setDate', testStartDate);
	// 			}
	// 			else {
	// 				endDateTextBox.val(dateText);
	// 			}
	// 		},
	// 		onSelect: function (selectedDateTime){
	// 			endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
	// 		}
	// 	});
	// 	endDateTextBox.datetimepicker({ 
	// 		timeFormat: 'HH:mm:00',
	// 		dateFormat: 'yy-mm-dd',
	// 		onClose: function(dateText, inst) {
	// 			if (startDateTextBox.val() != '') {
	// 				var testStartDate = startDateTextBox.datetimepicker('getDate');
	// 				var testEndDate = endDateTextBox.datetimepicker('getDate');
	// 				if (testStartDate > testEndDate)
	// 					startDateTextBox.datetimepicker('setDate', testEndDate);
	// 			}
	// 			else {
	// 				startDateTextBox.val(dateText);
	// 			}
	// 		},
	// 		onSelect: function (selectedDateTime){
	// 			startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
	// 		}
	// 	});
	// 	$('#given').onchange(function () {
	// 		$('#startson').val(this.value);
	// 	});
	// });
	</script>    
  <script type="text/javascript">
  $(document).ready(function() {
    // $('.rotate').css('height', $('.rotate').width());
  });
  </script>

@stop

@section('content')

<!--   <div class="row-fluid">
    <div class="col-md-3 col-sm-3 pull-right">
        <div class="row">
            <div class="col-md-offset-6 col-sm-offset-6 col-md-3 col-sm-3">
                <div class="h1">
                  <a href="{{ URL::to('/tasks/create') }}" title="Add new task">
                    <span class="glyphicon glyphicon-plus-sign pull-right"></span>
                  </a>
                </div>
            </div>
        </div>
    </div>

  </div>
 -->      <div style="clear:both;"></div>
  <div class="row">
<!--      <div class="col-md-5 col-sm-5">
        <div class="row">
            <div class="col-md-12">
                <span class="h2">Priority List</span>
            </div>
        </div>
    </div> -->
    <div class="row">
      <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li><span class="h3">Things to do (Priority List) </span></li>
            <li class="pull-right"><a data-toggle="tab" href="#completed"><span class="glyphicon glyphicon-ok-sign" style="font-size: 14px;"></span> Completed</a></li>
            <li class="active pull-right"><a data-toggle="tab" href="#current"><span class="glyphicon glyphicon-pushpin" style="font-size: 14px;"></span> Current</a></li>
            <li class="pull-right"><a href="{{ URL::to('/tasks/create') }}"><span class="glyphicon glyphicon-plus-sign" style="font-size: 14px;"></span> New</a></li>
        </ul>
      </div> 
    </div>
    <div class="row-fluid">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-12 tab-content">
            <div id="current" class="tab-pane fade in active">
              <br>
              {{-- <div class="panel panel-default"> --}}
                {{-- <div class="panel-body"> --}}
                  <ul class="errors">
                      @foreach($errors->all('<li>:message</li>') as $message)
                      {{ $message }}
                      @endforeach

                  </ul>                    
                  {{-- <span class="pull-right"><span class="h1"><a href="{{ URL::to('/tasks/create') }}" title="Add new task"><span class="glyphicon glyphicon-plus-sign pull-right"></span></a></span></span> --}}
                  {{ overdueItemsMessage($tasksOverDue) }}
                  {{-- {{ listPriorityItemsUngrouped($tasks) }} --}}
                  {{-- {{ ($pg->priorityGroups) }} --}}

                  {{ priorityGraph($prioritySectors, $tasksNotDue, $tasksOverDue) }}
                  {{-- {{ listPriorityGroups($pg->priorityGroups, $tasks, $overdue) }} --}}
                  
                {{-- </div> --}}
              {{-- </div> --}}
            </div>
            <!-- </div> -->
            <!-- <div class="row"> -->
            <!-- <div class="col-md-8"> -->
            <div id="completed" class="tab-pane fade">
              <br>
              @if ($complete)
                  <div class="panel panel-default">
                    <div class="panel-body">
                      @foreach ($complete as $c)
                        <p>{{ HTML::linkRoute('edit', $c->title, array($c->id)) }}</p>
                      @endforeach
                    </div>
                  </div>
                {{-- listPriorityGroups($pg->priorityGroups, $complete) --}}
                {{-- $pg->listPriorityGroups($pg->priorityGroups, $complete) --}}
              @endif                    
            </div>
            <!-- </div>       -->
            <!-- </div> -->
          </div>
        </div>
      </div>

    </div>
    <div class="row">
      <div class="col-md-9">                        
        <br>
        <div class="panel panel-default">
          <div class="panel-heading">
            <div class="title">Tags / Filters <a href="{{ URL::to('/prioritylist') }}">[clear]</a> </div>
          </div>
          <div class="panel-body">
            @foreach($tags as $tag)
            <a href="{{ URL::to('/prioritylist') }}?tag={{ $tag }}">[{{ $tag }}]</a> 
            @endforeach
          </div>
        </div>
      </div>
    </div>


@stop



<?php 
  function overdueItemsMessage($overdue) {
    if (count($overdue) > 0){
      $rv = '<div class="alert alert-warning alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
      $rv .= '<h4>You have overdue items</h4>';
      $rv .= '<ul><li><em>Make sure to realistically extend the deadlines of overdue items so they can be properly prioritized.</em></li></ul>';
      $rv .= '</div>';
    }
    return $rv;     
  }
  
  function priorityGraph($sectors, $tasks, $overdueTasks)
    {
      $priorityColor = ['success', 'info', 'warning', 'default'];
      $rowName = ['Important', 'Not Important'];
      $colName = ['Not Urgent', 'Urgent'];
      $i = 0;
      $rv = '
      <table class="sector_graph">
        <tr>
          <td></td>
          <td class="first_row"><div>Not Urgent</div></td>
          <td class="first_row">Urgent</td>
        </tr>
        <tr>
          <td class="first_col"><div class="rotate">'.$rowName[0].'</div></td>';
      foreach ($sectors as $sector) {
        $rv .= '
        <td class="data_col '.$priorityColor[0].'">';
        // $rv .= '<h4>'.$sector['name'].'</h4>';
        foreach ($overdueTasks as $overdueTask) {
          if ($sector['id'] == $overdueTask->priorityGrpId) {
            $rv .= '<div class="overdue_task">';
            $rv .= taskItem($overdueTask);
            $rv .= '</div>';
          }
        }
        foreach ($tasks as $task) {
          if ($sector['id'] == $task->priorityGrpId) {
            $rv .= '<div>';
            $rv .= taskItem($task);
            $rv .= '</div>';
          }
        }
        
        $rv .='
        </td>';
        $i++;
        if ($i%2 == 0){
          $rv .= '</tr>';
          if ($i < 4) {
            $rv .= '<tr class=""><td class="first_col"><div class="rotate">'.$rowName[1].'</div></td>';
          }
        }
      }
      $rv .= '
        </tr>
      </table>';
      return $rv;
    }  
  function listPriorityGroups($priorityGrps, $todos, $overdue)
  {
      $priorityColor = ['success', 'info', 'warning', 'default'];
      $rv = '<div class="row">';
      $i = 0;
      foreach ($priorityGrps as $priorityGrp) {
          $rv .= '
                <div class="col-md-6">
                  <div class="panel panel-'.$priorityColor[$i].'">';
          $rv .= priorityGrpItem($priorityGrp, $todos, $overdue);
          $rv .= '</div>
                </div>';
          $i++;
          if ($i%2 == 0){$rv .= '</div><div class="row">';}
      }
      $rv .= '</div>';
      return $rv;
  }

  function priorityGrpItem($priorityGrp, $todos, $overdue){
	$rv = '<div class="panel-heading">';
		$rv .= $priorityGrp['name'];
	$rv .= '</div>';
	$rv .= '<div class="panel-body">';
	  // $rv .= '<ul>';

    $rv .= listPriorityItems($todos, $priorityGrp['id'], $overdue);
    // $rv .= '</ul>';
	$rv .= '</div>';
	return $rv;
  }

  function listPriorityItems($todos, $priorityGrpId, $overdue)
  {
    usort($todos, 'sortByPriority');
    $rv = "";
    foreach ($todos as $todo) {
      $overdue = false;
      // echo  date("Y-m-d H:i:s");
      $now = strtotime( "now" );
      $due = strtotime($todo->due);
      if ( $now <= $due ) { $overdue = true; }
      // var_dump($now);
      // echo date("Y-m-d H:i:s")." <br> ";
      // var_dump($due);
      // var_dump($overdue);
      // echo $todo->title;
      // echo " <br> ";// die();
      
      $rv .= '<div class="'.$retVal = ($overdue) ? "overdue-item" : null.'">'; 
      $rv .= priorityListItem($todo, $priorityGrpId);
      $rv .= '</div>'; 
    }
    return $rv;
  }

  function listPriorityItemsUngrouped($todos)
  {
    usort($todos, 'sortByPriority');
    // echo '<pre>';print_r($todos);die();
    // if (isset($todos)){
      $rv = "<h4>Current</h4>";
      foreach ($todos as $todo) {
        $rv .= priorityListItemUngrouped($todo);
      }
      return $rv;
  }

  function sortByPriority($a, $b)
  {
      return $a->priority < $b->priority;
  }
  function priorityListItemUngrouped($todo)
  {
    $rv = "";
    $rv .= taskItem($todo);
    return $rv;
  }
  function priorityListItem($todo, $priorityGrpId)
  {
    //echo "<pre>";
    //  print_r($todo);
    //  die();
      $rv = "";
  	if ($todo->priorityGrpId == $priorityGrpId){
      $rv .= taskItem($todo);
    }
	return $rv;
  }

  function formatHrs($time){
    $t['days'] = floor($time/24);
    $t['hours'] = floor($time - ($t['days']*24));
    $t['minutes'] = floor(($time - ($t['days']*24) - ($t['hours'])) * 60);
    return $t;
  }
  function showTime($t)
  {
    $rv = '';
    if ($t['days'] > 0) {
      $rv .= $t['days'].'D';
    }
    if ($t['hours'] > 0) {
      $rv .= $t['hours'].'h';
    }
    if ($t['minutes'] > 0) {
      $rv .= $t['minutes'].'m';
    }
    return $rv;
  }

  function taskItem($todo)
  {
        $time = $todo->allottedtime;
        $a_time = formatHrs($todo->allottedtime);
        $days = $a_time['days'];
        $hours = $a_time['hours'];
        $minutes = $a_time['minutes'];
     
        $url = 'https://www.google.com/calendar/render?action=TEMPLATE'; // https://www.google.com/calendar/render?action=TEMPLATE&text=[progressum][reproductivesolutionsja]+Go+through+the+content+we+got+from+them&dates=2015-05-19T06:23:48+0000/2015-05-19T06:23:48+0000&details=Go+through+the+content,+sort+and+upload+to+the+form+in+e-dasher&location&pli=1&sf=true&output=xml#main_7
        $url .= '&text='.$todo->title;
        $url .= '&dates='.date('Ymd\THi00\Z').'/'.date('Ymd\THi00\Z');
        $url .= '&details='.$todo->description;
        $url .= '&location=';
        $url .= '&sf=true';
        $url .= '&output=xml';
        //echo $url;

        $rv = '<div class="priorityListItem">';
        $rv .= '<strong><a href="'.URL::route('edit', $todo->id).'" class="itemTitle">'.$todo->title.'</a></strong> ';
        $rv .= '<span class="itemMeta">('.($todo->progress*100).'%)<br>';
        $rv .= ' Allotted: '.showTime($a_time);
        if (isset($todo->overdue)) {
          $rv .= ' Due: '.date("d/m/y g:i a", strtotime($todo->due)).'. ';
        } else {
          $rv .= ' Available: '.showTime(formatHrs($todo->timeavail));
          if ($todo->allottedtime > $todo->timeneeded) {
            $rv .= ' Needed: '. showTime(formatHrs($todo->timeneeded));
          }
        }
        
        // $rv .= ' Importance Score: '. $todo->importance;
        // $rv .= ' Urgency Score: '. $todo->urgency;
        // $rv .= ' Priority Score: '. $todo->priority;
        $rv .= ' </span>';
        $rv .= '<a href="'.$url.'", target="_blank" class="btn btn-primary btn-xs" style="display:inline-block" title="Schedule in Google Calendar">Google <span class="glyphicon glyphicon-calendar"></span></a> ';
        $rv .= ' <div style="display:inline-block">'.' <a href="'.$url.'" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#calendar-reminder-Modal-'.$todo->id.'" title="Schedule in your favorite calendar"><i class="glyphicon glyphicon-time"></i><i class=" glyphicon glyphicon-calendar"></i></a>'.'</div>';
        $rv .= ' <div style="display:inline-block">'.' <a href="#" title="Schedule an email reminder" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#email-reminder-Modal-'.$todo->id.'"><i class=" glyphicon glyphicon-time"></i><i class=" glyphicon glyphicon-envelope"></i></a>'.'</div>';
        $rv .= ' <div style="display:inline-block">';
        $rv .= Form::open(array('route' => array('tasks.update', $todo->id), 'class' => 'form-inline', 'role'=>"form"));
        $rv .= Form::hidden('title', $todo->title);
        $rv .= Form::hidden('importance', $todo->importance);
        $rv .= Form::hidden('due', $todo->due);
        $rv .= Form::hidden('days', $days);
        $rv .= Form::hidden('hours', $hours);
        $rv .= Form::hidden('minutes', $minutes);
        $rv .= Form::hidden('progress', 1);
        $rv .= Form::hidden('user_id', $todo->user_id);
        $rv .= Form::button('<span class="glyphicon glyphicon-ok"></span>', ['class' => 'btn btn-primary btn-xs', 'title' => 'Mark complete', 'type' => 'submit']);
        // $rv .= Form::submit('<span class="glyphicon glyphicon-ok"></span>', ['class' => 'btn btn-primary btn-xs', 'title' => 'Mark complete']);
        $rv .= Form::close();
        $rv .= '</div>';
        $rv .= '
          <div class="modal fade" id="calendar-reminder-Modal-'.$todo->id.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Schedule work time in your favourite calendar app</h4>
                </div>
                <div class="modal-body">';
        $rv .= Form::open(array('route' => 'downloadcal', 'class'=>'form', 'id'=>'event-capture-form'));
        $rv .= Form::hidden('uid', $todo->id.date("YmdHis") );
        $rv .= Form::hidden('summary', $todo->title );
        $rv .= Form::hidden('description', $todo->description);

        $rv .= '<div class="form-group">';
        $rv .= Form::label('start', 'Set start date & time:', []);
        $rv .= Form::text('start', null, array('placeholder' => 'Click to select date and time', 'class'=>'form-control', 'id'=>'start-time'.$todo->id));
        $rv .= '</div>';
        $rv .= '<div class="form-group">';
        $rv .= Form::label('end', 'Set end date & time:', []);
        $rv .= Form::text('end', null, array('placeholder' => 'Click to select date and time', 'class'=>'form-control', 'id'=>'end-time'.$todo->id));
        $rv .= '</div>';
        $rv .= '<div class="form-group">';
        $rv .= Form::label('description', 'Description:', []);
        $rv .= Form::textarea('description', $todo->description, array('placeholder' => 'Add a note to the body of message (optional)', 'class'=>'form-control', 'size' => '30x4'));
        $rv .= '</div>';
        $rv .= '<div class="form-group">';
        $rv .= Form::label('location', 'Location:', []);
        $rv .= Form::text('location', null, array('placeholder'=>'Set the event location (optional)', 'class' => 'form-control'));
        $rv .= '</div>';
        $rv .= Form::submit('Set', array('style'=>'display:none', 'id'=>'event-capture-submit-form'));
        $rv .= Form::close();
        $rv .= '</div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <label for="event-capture-submit-form" class="btn btn-primary">Set</label>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->';
        $rv .= '
          <div class="modal fade" id="email-reminder-Modal-'.$todo->id.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Select date and time for the email to be sent</h4>
                </div>
                <div class="modal-body">';
                  $rv .= Form::open(array('route' => 'emailreminder', 'class'=>'form', 'id'=>'emailreminder'));
                  $rv .= Form::hidden('user_id', $todo->user_id );
                  $rv .= Form::hidden('id', $todo->id );
                  $rv .= Form::hidden('title', $todo->title );
                  $rv .= Form::hidden('description', $todo->description);
                  $rv .= Form::hidden('due', $todo->due);
                  $rv .= Form::hidden('allottedtime', $todo->allottedtime);
                  $rv .= Form::hidden('progress', $todo->progress);

                  $msg = '';
                  // $msg = 'Title: '. $todo->title.'';
                  $msg .= (!empty($todo->description)) ? 'Description: '.$todo->description.'; ' : '' ;
                  $msg .= '';
                  $msg .= 'Progress: '. ($todo->progress*100) .'%; ';
                  $msg .= 'Due on: '. $todo->due . '; ';
                  $msg .= 'Time allotted: '. $todo->allottedtime .' hour(s); ';

                  $rv .= '<div class="form-group">Recipients:'.Form::text('recipients', Auth::user()->email, array('placeholder' => 'Additional recipients (optional) - separate by comma', 'class'=>'form-control')).'</div>';
                  $rv .= '<div class="form-group">Subject:'.Form::text('subject', 'LzyReminder: '.$todo->title, array('placeholder' => 'Subject of email', 'class'=>'form-control')).'</div>';
                  $rv .= '<div class="form-group">Message:'.Form::textarea('message', $msg, array('placeholder' => 'Add a note to the body of message (optional)', 'class'=>'form-control', 'size' => '30x4')).'</div>';
                  // $rv .= '<div class="row">';
                    $rv .= '<div class="form-group">Delivery date and time (Approximate)'.Form::text('sendtime', null, array('placeholder' => 'Click to select date and time', 'class'=>'form-control', 'id'=>'sendtime'.$todo->id)).'</div>';
                    
                  $rv .= Form::submit('Set', array('style'=>'display:none', 'id'=>'emailreminder-submit-form'));
                  $rv .= Form::close();
                $rv .= '</div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <label for="emailreminder-submit-form" class="btn btn-primary">Set</label>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->';

        $rv .= '</div>';
         
        $rv .= '<script>';
            $rv .= '$("#start-time'.$todo->id.'").datetimepicker({';
                $rv .= 'step: 15,';
                $rv .= 'format:"Y-m-d H:i",';
                $rv .= 'minDate:"0",';
                $rv .= 'minTime:"'.date("H:i:s").'"';
            $rv .= '});';
            $rv .= '$("#end-time'.$todo->id.'").datetimepicker({';
                $rv .= 'step: 15,';
                $rv .= 'format:"Y-m-d H:i",';
                $rv .= 'minDate:"0",';
                $rv .= 'minTime:"'.date("H:i:s").'"';
            $rv .= '});';
            $rv .= '$("#sendtime'.$todo->id.'").datetimepicker({';
                $rv .= 'step: 5,';
                $rv .= 'format:"Y-m-d H:i:s",';
                $rv .= 'minDate:"0",';
                $rv .= 'minTime:"'.date("H:i:s").'"';
            $rv .= '});';
        $rv .= '</script>';
        return $rv;
      
    }
?>