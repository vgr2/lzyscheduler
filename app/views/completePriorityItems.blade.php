<!-- C:\xampp183\htdocs\lzf\app/views/thingtodos/index.blade.php -->
<?php $page = "Priority List"; ?>
@extends('layouts.main-bootstrap-fixed')

@section('head')
    @parent
	<!-- <link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" /> -->
	{{ HTML::style('jquery-ui-1.11.0.custom/jquery-ui.css', array('media' => 'all')) }}
    {{ HTML::style('timepicker/jquery-ui-timepicker-addon.css') }}
    
    {{ HTML::script('jquery-ui-1.11.0.custom/jquery-ui.js') }}
	<!-- <script src="{{ URL::asset('datetimepicker/jquery.js') }}"></script> -->
	{{ HTML::script('timepicker/jquery-ui-timepicker-addon.js') }}

    <style type="text/css">
      .taskContainer{
        border: 1px solid antiquewhite;
        font-family: Gill, Helvetica, sans-serif;
      }
      .groupHeader{
        font-weight: bold;
        font-size: 14px;
      }
      .groupTasks{margin: 2px;}
      .groupTasks p{margin: 2px;}
    </style>
	<script type="text/javascript">
	 $(document).ready(function(){
		var startDateTextBox = $('#given');
		var endDateTextBox = $('#due');

		startDateTextBox.datetimepicker({ 
			timeFormat: 'HH:mm:00',
			dateFormat: 'yy-mm-dd',
			onClose: function(dateText, inst) {
				if (endDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						endDateTextBox.datetimepicker('setDate', testStartDate);
				}
				else {
					endDateTextBox.val(dateText);
				}
			},
			onSelect: function (selectedDateTime){
				endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
			}
		});
		endDateTextBox.datetimepicker({ 
			timeFormat: 'HH:mm:00',
			dateFormat: 'yy-mm-dd',
			onClose: function(dateText, inst) {
				if (startDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						startDateTextBox.datetimepicker('setDate', testEndDate);
				}
				else {
					startDateTextBox.val(dateText);
				}
			},
			onSelect: function (selectedDateTime){
				startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
			}
		});
		$('#given').onchange(function () {
			$('#startson').val(this.value);
		});
	});
	</script>    

@stop

@section('content')
	
	
   	<!-- <div class="clearfix"></div> -->
    <div class="h2">
    	Priority List
    	<small class="pull-right">
    		Completed Items<br>
    		<small class="clearfix btn btn-default pull-right">{{ HTML::link('/prioritylist', '<< Back', ['class'=>'h4 btn btn-info btn-xs pull-right', 'style'=>'margin: -10px 0 15px;']) }}</small>
    	</small>
    </div>
    
	
    
    <?php // echo $pg->listTasksByPriority($pg->tasks); ?>
    <?php 
    $rv = '<ul>';
	  foreach ($pg->tasks as $todo) {
	    // if ($todo['priorityGrpId'] == $priorityGrp['id']){
	      $rv .= '<li>';
	        $rv .= '<strong><a href="'.URL::route('edit', $todo->id).'">'.$todo->title.'</a></strong> ';
	        // if ($todo['overdue'] == 'yes'){ $rv .= '<small class="error">(overdue - <a href="'.URL::route('edit', $todo->id).'">extend deadline</a>)</small>';}
	        $rv .= '<br>Progress: '.($todo->progress*100).'% '; 
	        $rv .= ' Due on: '.$todo->due.'. '; 
	        // $rv .= ' Priority Score: '. $todo['priority'];
	      $rv .= '</li>';
	    // }
	  }
  	$rv .= '</ul>'; 
  	echo $rv;     
    ?>

@stop