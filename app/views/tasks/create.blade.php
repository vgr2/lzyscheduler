<?php $page = "New Task"; ?>
@extends('layouts.main-bootstrap-fixed')

@section('head')
    @parent
	<!-- <link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" /> -->
        {{--<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>--}}
        <!--{{-- HTML::script('timepicker/jquery-ui-timepicker-addon.js') --}}-->
	{{-- HTML::script('datetimepicker/jquery.js') --}}
        {{ HTML::style('datetimepicker/jquery.datetimepicker.css') }}
        {{ HTML::script('datetimepicker/jquery.datetimepicker.js') }}
        
        {{ HTML::script('js/tinymce/tinymce.min.js') }}
        {{-- <script>tinymce.init({selector:'textarea'});</script> --}}
        {{-- Messes up the link to google calendar.  A work around would be nice --}}

        <style>
            .half-width-float{
                width: 48%; float: left;
            }
        </style>

@stop

@section('content')

    @foreach ($errors->all() as $error)

        <p class="alert alert-warning">{{ $error }}</p>
    
    @endforeach

	<div class="h3">New Thing to do</div>	
 
	{{ Form::open() }}
        @include('tasks.formfields')
        <p>
            <div style="clear:both; margin-top: 10px;">
                {{ Form::hidden('user_id', $user_id) }}
                {{ Form::submit('Save New', ['class' => 'btn btn-default']) }}
            </div>
        </p>
	{{ Form::close() }}
    <script>
        $('#due').datetimepicker({
            step: 5,
            format:'Y-m-d H:i',
        });
    </script>
<!--    <script type="text/javascript">
     $(document).ready(function(){
        var timeDue = $('#due');
                
                timeDue.datetimepicker({
                    timeFormat: 'HH:mm:00',
                    stepMinute: 15,
                    dateFormat: 'yy-mm-dd'
                });

    });
    </script>    -->
@stop

