<?php


    // var_dump($task);die();
    if (isset($task)){
        $time = $task['allottedtime'];
        $days = floor($time/24);
        $hours = floor($time - ($days*24));
        $minutes = floor(($time - ($days*24) - ($hours)) * 60);
    } else {
        $days = 0;
        $hours = 0;
        $minutes = 0;
    }

    // echo 'd:' . $days . ', h: '.$hours. ', m: '. $minutes; 
?>

<div class="row" style="margin-top: 10px;">
    <div class="col-md-12">
        <div class="row">
{{--            <div class="col-md-1">{{ Form::label('title', 'What: ', array() ) }}</div>--}}
            <div class="col-md-12">{{ Form::text('title', null, array('placeholder' => 'Enter something you have to do.', 'class' => 'form-control')) }}</div>
        </div>
    </div>
<!--			<br>-->
{{-- Form::label('location', 'Where: ') --}}
{{-- Form::text('location', '', array('placeholder' => '', 'class' => 'form-control')) --}}
</div>
{{--		<div>
			{{ Form::label('allday', 'All day:', []) }}
			{{ Form::checkbox('allday') }}
			{{ Form::label('recur', 'Repeat:', []) }}
			{{ Form::checkbox('recur') }}
		</div>--}}
<div class="row" style="margin-top: 10px;">
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-12">
                <h4>Description</h4>
                {{-- {{ Form::label('description', 'Description:', array() ) }} --}}

                {{-- <h4>Description</h4> --}}
                {{ Form::textarea('description', null, array('placeholder' => 'Description of the thing to be done. (optional)', 'rows' =>'13', 'class' => 'form-control')) }}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="row">
            <div class='col-xs-12'>
                <h4>Time Needed</h4>
                <div class="row">
                    <div class="col-md-4 col-xs-4">
                        {{ Form::label('days', 'Days:', array() ) }}
                        {{ Form::number('days', $days, array('placeholder' => '', 'class' => 'form-control', 'min' => '0', 'pattern' => '\d+')) }}
                    </div>
                    <div class="col-md-4 col-xs-4">
                        {{ Form::label('hours', 'Hours:', array() ) }}
                        {{ Form::number('hours', $hours, array('placeholder' => '', 'class' => 'form-control', 'min' => '0', 'pattern' => '\d+')) }}
                    </div>
                    <div class="col-md-4 col-xs-4">
                        {{ Form::label('minutes', 'Minutes:', array() ) }}
                        {{ Form::number('minutes', $minutes, array('placeholder' => '', 'class' => 'form-control', 'min' => '0', 'pattern' => '\d+')) }}
                    </div>
                </div>

                {{-- Form::label('days', 'Days:', array() ) --}} <!--<small>(estimate in hours)</small>-->
                {{-- Form::number('days', null, array('placeholder' => '', 'class' => 'form-control', 'min' => '0', 'pattern' => '')) --}}
            </div>
            <div class='col-xs-12'>
                
                <div class="row" style="margin-top: 1em;">

                    <div class="col-md-12">
                        <h4>Due By</h4>
                        {{ Form::label('due', 'Date and Time:', []) }}
                        {{ Form::text('due', null, array('placeholder' => 'Click to select date and time', 'class' => 'form-control')) }}
                    </div>
                </div>


            </div>
        </div>

        <div class="row" style="margin-top: 1em;">
            <div class='col-md-6'>
                <h4>Importance <small>( <output id="importancevalue" style="display: inline;"><?php if(!empty($task)){echo $task->importance;} else {echo "1";} ?></output> )</small></h4>
                <div class="row">
                    <div class="col-md-12">
                        <input type="range" min="1" max="10" value="<?php if(!empty($task)){echo $task->importance;} else {echo "1";} ?>" step="1" name="importance" id="importance" onchange="importancevalue.value=value">
                    </div>
                </div>
            </div>
            <div class='col-md-6'>
                <h4>Progress  <small>( <output id="progressvalue" style="display: inline;"><?php if(!empty($task)){echo $task->progress * 100;} else {echo "0";} ?></output>% )</small></h4>
                {{-- Form::label('progress', 'Progress: ',[]) --}} <!--<small>(0 - 100%)</small>-->
                {{--<div style="margin-top: 8px;">--}}
                <!-- {{-- Form::text('progress', null, array('data-slider' => 'true')) --}} -->
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <input type="range" min="0" max="1" value="<?php if(!empty($task)){echo $task->progress;} else {echo "0";} ?>" step="0.01" name="progress" id="progress" onchange="progressvalue.value=value*100">
                    </div>
                </div>
                {{--</div>--}}
            </div>
        </div>
    </div>
</div>
<!-- <div> -->
