<?php $page = "Edit Task"; ?>
@extends('layouts.main-bootstrap-fixed')

@section('head')
    @parent
            <!-- <link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" /> -->
    <script src="http://code.jquery.com/jquery-1.11.0.min.js" xmlns="http://www.w3.org/1999/html"></script>
    <!--{{-- HTML::script('timepicker/jquery-ui-timepicker-addon.js') --}}-->
    {{-- HTML::script('datetimepicker/jquery.js') --}}
    {{ HTML::style('datetimepicker/jquery.datetimepicker.css') }}
    {{ HTML::script('datetimepicker/jquery.datetimepicker.js') }}

    {{ HTML::script('js/tinymce/tinymce.min.js') }}
    {{-- <script>tinymce.init({selector:'textarea'});</script>--}}
    {{-- Messes up the link to google calendar.  A work around would be nice --}}
    
    
    <script language="javascript">
        function checkMe() {
            if (confirm("Are you sure")) {
                document.getElementById("deleteItem").submit();
                return true;
            } else {
                alert("Clicked Cancel");
                return false;
            }
        }
    </script>
    <style>
        .half-width-float{
            width: 48%; float: left;
        }
    </style>

@stop

@section('content')

    {{ Form::open(array('url' => 'tasks/' . $task->id, 'class' => 'pull-right', 'id' => 'deleteItem')) }}
    {{ Form::hidden('_method', 'DELETE') }}
    {{ Form::submit('Delete', array('class' => 'btn btn-warning', 'onClick' => 'return checkMe()')) }}
    {{ Form::close() }}

    <div class="h3">Edit this Thing to do</div>

    @foreach ($errors->all() as $error)

        <p class="error">{{ $error }}</p>

    @endforeach

    {{ Form::model($task, array('route' => array('tasks.update', $task->id), 'class'=>'form-horizontal')) }}
    @include('tasks.formfields')
    <div class="row" style="margin-top: 10px;">
        <div class="col-md-12">
            <div class="row">
                {{ Form::hidden('user_id', $user_id) }}
                <div class="col-md-3">{{ Form::submit('Save Changes', ['class'=>'btn btn-success']) }}</div>
                {{-- TODO: Add ability to schedule a task reminder/alert via specified email - changeable default of owner email--}}
                <div class="col-md-3">
                {{-- HTML::link('','Set Email Reminder', array('class' => 'btn btn-default', 'disabled' => 'disabled'),false ) --}}
                <!-- <a href="#" class="btn btn-large btn-primary" rel="popover" data-content='<form action=""><input id="!reminder_time" type="text"/><input type="submit" value="Set"></form>' data-placement="top" data-original-title="Set Email Reminder">Set Email Reminder</a> -->
                </div>
                {{--// TODO: Add ability to schedule work timr on the task in Google calendar--}}
                <div class="col-md-6">

                    {{--array('class' => 'btn btn-default', 'target'=>"_blank", 'rel'=>'nofollow') ) }}--}}
                </div>

            </div>
        </div>
    </div>

    {{ Form::close() }}
    <script>
        $('#due').datetimepicker({
            step: 15,
            format:'Y-m-d H:i',
        });
        $('#reminder_time').datetimepicker({
            step: 15,
            format:'Y-m-d H:i',
        });
    </script>
    <!--    <script type="text/javascript">
         $(document).ready(function(){
            var timeDue = $('#due');

                    timeDue.datetimepicker({
                        timeFormat: 'HH:mm:00',
                        stepMinute: 15,
                        dateFormat: 'yy-mm-dd'
                    });

        });
        </script>    -->
@stop

