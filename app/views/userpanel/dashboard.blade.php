<?php $page = "Dashboard"; ?>
@extends('layouts.main-bootstrap-fixed')

@section('head')
    @parent
    
	<script type="text/javascript">
	function update(user_id, timezone){

		if (window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {  // code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}

		xmlhttp.onreadystatechange=function() {
		    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		      document.getElementById("tbl").innerHTML=xmlhttp.responseText;
		    }
		}

		xmlhttp.open("GET", "<?= action('SettingsController@update'); ?>?user_id="+user_id+"&timezone="+timezone, true );
		xmlhttp.send();

	}
	</script>    
    
@stop


@section('content')

<div class="maincontent">
    <h1>{{ Confide::user()->username }}</h1>
    <div class="well">
        <p><b>E-mail:</b> {{ Confide::user()->email }}</p>
        <hr>
        <p><strong>Time zone:</strong> {{ Form::select('timezone', DateTimeZone::listIdentifiers()) }} </p>

        <!--Add summary of the days activities inclusive of events and tasks-->
        <!--Add statistics charts - e.g. percentage of completed vs incomplete vs overdue tasks-->
    </div>
 </div>

@stop

