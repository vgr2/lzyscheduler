<?php
/**
 * Created by PhpStorm.
 * User: Valdeck
 * Date: 3/16/2015
 * Time: 9:07 PM
 */
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <h2>{{$task->title}}</h2>
    @if(isset($task->description))
        <p><strong>Description: </strong>{{ $task->description }}</p>
    @endif
    <p><strong>Created: </strong> {{ $task->created_at }}</p>
    <p><strong>Due: </strong> {{ $task->due }}</p>
    <p><strong>Allotted time: </strong> {{ $task->allottedtime }}</p>

</body>
</html>
