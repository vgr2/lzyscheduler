@extends('layouts.main')

@section('head')
    @parent
	<!-- <link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" /> -->
	{{ HTML::style('jquery-ui-1.11.0.custom/jquery-ui.css', array('media' => 'all')) }}
    {{ HTML::style('timepicker/jquery-ui-timepicker-addon.css') }}
    
    {{ HTML::script('jquery-ui-1.11.0.custom/jquery-ui.js') }}
	<!-- <script src="{{ URL::asset('datetimepicker/jquery.js') }}"></script> -->
	{{ HTML::script('timepicker/jquery-ui-timepicker-addon.js') }}

@stop

@section('content')
  <h2 class="h2">Password Reset</h2>

  <div>
      To reset your password, complete this form: {{ URL::to('password/reset', array($token)) }}.<br/>
      This link will expire in {{ Config::get('auth.reminder.expire', 60) }} minutes.
  </div>
@stop