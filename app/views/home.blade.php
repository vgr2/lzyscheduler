<?php $page = "Home"; ?>
@extends('layouts.main-bootstrap-fixed')

@section('head')
    @parent

@stop

@section('content')
    <h3>LzyScheduler - for the things you have to do</h3>
    {{--<h2>Welcome</h2>--}}

    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <p>LzyScheduler helps you by taking all the things you have to do, prioritizes and organizing them based on importance and urgency.</p>

                {{--<p>LzyScheduler helps you by taking all the things you have to do, organizing and prioritizing them, into a daily, weekly or monthly schedule.</p>--}}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">About - not so long version</div>
                <div class="panel-body">
                    <p>When we were trying to describe what has become this app,
                        we had a bit of trouble categorising exactly what we were trying to build.
                        It eventually came down to "helping busy people prioritise and organise the things they have to do".</p>
                    <p>
                        During this phase we realized that things to do generally fall into two categories:
                        they either have a fixed start and end time or they may not have a set start time
                        (read 'flexible start time'), but they do have deadlines and the actual activity could take
                        place at any time between the time given and the deadline / time due.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title">Usage Tips</div>
                </div>
                <div class="panel-body">
                    <h4>Adding a new item</h4>
                    <ol>
                        <li>Select "Things to do" from the menu</li>
                        <li>Click "New"</li>
                        <li>Add the title</li>
                        <li>Select the date and time due</li>
                        <li>Set the importance on a scale of 1 - 10</li>
                        <li>Set the progress to whatever you feel has been made</li>
                        <li>Click the "Save Now" button</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>


    I was hoping for a grandiose speech about our dreams that this little software will change the world. Sigh. Sadly and maybe for the best... I'm stumped.</p>
        

    <h3>Not so long version</h3>

        <h3>Short Version</h3>
        <p>The short of it is this, the time allotted for events is the same as the time difference between the start and end times of the thing to do. The time allotted for a task however is often less than the time when the start and end times</p>
        
   





@stop