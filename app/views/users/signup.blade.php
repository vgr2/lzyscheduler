<?php $page = "Signup"; ?>
@extends('layouts.main-bootstrap-fixed')

@section('head')
    @parent
    
    
@stop


@section('content')

<div class="maincontent">
  <div class="h2">Register</div>
  <div style="padding: 10px;">
    {{-- Renders the signup form of Confide --}}
    {{ Confide::makeSignupForm()->render(); }}
  </div>
</div> 

@stop

