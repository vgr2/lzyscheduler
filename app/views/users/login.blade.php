<?php $page = "Login"; ?>
@extends('layouts.main-bootstrap-fixed')

@section('head')
    @parent
    
    
@stop


@section('content')

<div class="maincontent">
  <h2>Login</h2>
  <div style="padding: 10px;">
    {{-- Renders the login form of Confide --}}
    {{ Confide::makeLoginForm()->render(); }}
    <a href="{{ URL::to('/users/create') }}" class="btn btn-default" style="margin:2px 0 2px 0">Register</a>
  </div>
</div> 

@stop

