<?php $page = "Login"; ?>
@extends('layouts.main-bootstrap-fixed')

@section('content')

	@foreach ($errors->all() as $error)

		<p class="error">{{ $error }}</p>
	
	@endforeach
 	<div style="width:60%; margin:2px;">
	{{ Form::open(array('autocomplete' => 'off')) }}

		{{ Form::text('username', '', array('placeholder' => 'Username', 'class' => 'form-control pull-left', 'style' => 'width:48.5%; margin: 0 2px 2px 0;')) }} 
		{{ Form::password('password', array('placeholder' => 'Password', 'class' => 'form-control pull-right', 'style' => 'width:48.5%; margin: 0 0 2px 2px;')) }}
		<div class="clearfix"></div>
		<p> {{ Form::submit('Sign in', array('class' => 'btn btn-default form-control', 'style' => 'margin:2px 0 2px 0')) }} <a href="#" class="btn btn-default form-control" style="margin:2px 0 2px 0">Register</a> </p>

	{{ Form::close() }}
	</div>

@stop