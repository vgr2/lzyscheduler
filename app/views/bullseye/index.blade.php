<?php $page = "Bulls-Eye List"; ?>
@extends('layouts.main-bootstrap-fixed')

@section('head')
    @parent

<script type="text/javascript">
function update(task_id, status){

	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {  // code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.onreadystatechange=function() {
	    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
	      document.getElementById("tbl").innerHTML=xmlhttp.responseText;
	    }
	}

	xmlhttp.open("GET", "<?= action('HomeController@updateStatus'); ?>?task_id="+task_id+"&status="+status, true );
	xmlhttp.send();

}
</script>

<style>
	th {
		text-align: center;
	}
	th .title, .entry_title {
		text-align: left;
		/*width: 50%;*/
	}
	.entry_status {
		text-align: center;
		width: 12%;
	}
</style>

@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		<ul class="nav nav-tabs">
		    <li><span class="h3">Bulls Eye Manager </span></li>
		    <li class="pull-right"><a data-toggle="tab" href="#completed"><span class="glyphicon glyphicon-ok-sign" style="font-size: 14px;"></span> Completed</a></li>
		    <li class="active pull-right"><a data-toggle="tab" href="#current"><span class="glyphicon glyphicon-pushpin" style="font-size: 14px;"></span> Current</a></li>
		    <li class="pull-right"><a href="{{ URL::to('/tasks/create') }}"><span class="glyphicon glyphicon-plus-sign" style="font-size: 14px;"></span> New</a></li>
		</ul>
	</div> 
</div>
    
<br>
<div id="tbl">

    {{ $tbl }}

</div> <!-- end tbl -->



@stop

<?php 
 ?>
