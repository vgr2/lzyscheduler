<?php $page = "New Task"; ?>
@extends('layouts.main-bootstrap-fixed')

@section('head')
    @parent
	<!-- <link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" /> -->
	{{ HTML::style('jquery-ui-1.11.0.custom/jquery-ui.css', array('media' => 'all')) }}
    {{ HTML::style('timepicker/jquery-ui-timepicker-addon.css') }}
    
    {{ HTML::script('jquery-ui-1.11.0.custom/jquery-ui.js') }}
	<!-- <script src="{{ URL::asset('datetimepicker/jquery.js') }}"></script> -->
	{{ HTML::script('timepicker/jquery-ui-timepicker-addon.js') }}

	<script type="text/javascript">
	 $(document).ready(function(){
		var startDateTextBox = $('#given');
		var endDateTextBox = $('#due');

		startDateTextBox.datetimepicker({ 
			timeFormat: 'HH:mm:00',
			stepMinute: 15,
			dateFormat: 'yy-mm-dd',
			onClose: function(dateText, inst) {
				if (endDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						endDateTextBox.datetimepicker('setDate', testStartDate);
				}
				else {
					endDateTextBox.val(dateText);
				}
			},
			onSelect: function (selectedDateTime){
				endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
			}
		});
		endDateTextBox.datetimepicker({ 
			timeFormat: 'HH:mm:00',
			stepMinute: 15,
			dateFormat: 'yy-mm-dd',
			onClose: function(dateText, inst) {
				if (startDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						startDateTextBox.datetimepicker('setDate', testEndDate);
				}
				else {
					startDateTextBox.val(dateText);
				}
			},
			onSelect: function (selectedDateTime){
				startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
			}
		});
		$('#given').onchange(function () {
			$('#startson').val(this.value);
		});
	});
	</script>    

@stop

@section('content')

	<div class="h2">Create a Thing To Do</div>

	@foreach ($errors->all() as $error)

		<p class="error">{{ $error }}</p>
	
	@endforeach
	
 
	{{ Form::open() }}
		<p>
			{{ Form::label('title', 'What: ') }}
			{{ Form::text('title', '', array('placeholder' => 'Enter something you have to do.')) }}
			<br>
			{{ Form::label('location', 'Where: ') }}
			{{ Form::text('location', '', array('placeholder' => '')) }}
		</p>
		<p>
			{{ Form::label('allday', 'All day:') }}
			{{ Form::checkbox('allday') }}
			{{ Form::label('recur', 'Repeat:') }}
			{{ Form::checkbox('recur') }} 			
		</p>
		<p>
			{{ Form::label('description', 'Description:') }} <br>
			{{ Form::textarea('description', null, array('placeholder' => 'Description of the thing to be done. (optional)', 'rows' =>'3')) }}

		</p>
		<p>
			{{ Form::label('importance', 'Importance: ')}}
			{{ Form::text('importance', null, array(
					'data-slider' => 'true', 
					'data-slider-range' => '1,10',
					'data-slider-step' => '1'
				)) 
			}}
			<br> 
			<small>(on a scale of 1 - 10)</small>
		</p>
		<p>
		<!-- </p>
		<p> -->
			{{ Form::label('given', 'Start:') }}
			{{ Form::text('given', null, array('placeholder' => 'Click to select date')) }}
			{{ Form::label('due', 'End:') }}
			{{ Form::text('due', null, array('placeholder' => 'Click to select date')) }}
		</p>
		<p>
			{{ Form::label('allottedtime', 'Time Allotted:') }}
			{{ Form::text('allottedtime', null, array('placeholder' => '')) }}<br>
			<small>(Expected length of task/event in hours)</small>
		</p>
		<p>
			{{ Form::label('progress', 'Progress')}}: 
			{{ Form::text('progress', null, array('data-slider' => 'true')) }}
		</p>
		<div id="repeats">
			<fieldset>
			<legend><strong>Repeats</strong></legend>
			<p id="every">
				
				Repeat {{ Form::label('every', 'every ') }}

				{{ Form::selectRange('every', 1, 30) }}
			<!-- </p>
			<p class="recurtype"> -->
				{{ Form::select('recurtype', [
					'day' => 'Day',
					'week' => 'Week',
					'month' => 'Monthly',
					'year' => 'Year'
				]) }} period.
				
			</p>
			<p id="weekly">
				{{ Form::label('sun','Su') }}
				{{ Form::checkbox('sun') }}
				{{ Form::label('mon','Mo') }}
				{{ Form::checkbox('mon') }}
				{{ Form::label('tue', 'Tu') }}
				{{ Form::checkbox('tue') }}
				{{ Form::label('wed', 'We') }}
				{{ Form::checkbox('wed') }}
				{{ Form::label('thu', 'Th') }}
				{{ Form::checkbox('thu') }}
				{{ Form::label('fri', 'Fr') }}
				{{ Form::checkbox('fri') }}
				{{ Form::label('sat', 'Sa') }}
				{{ Form::checkbox('sat') }}
			</p>
			<p id="monthly">
				{{ Form::radio('monthlyrepeaton', 'dayofmonth') }} day of month <strong>or</strong> {{ Form::radio('monthlyrepeaton', 'dayofweek') }} day of week
			</p>
			<p id="startson">
				{{ Form::label('startson', 'Starts on: ') }}
				{{ Form::text('startson', '', ['readonly' => 'readonly']) }}
			</p>
			<p id="ends">
				Ends:<br>
				{{ Form::radio('ends', 'never') }} Never <br>
				{{ Form::radio('ends', 'after') }} After {{ Form::text('endsafter') }} occurrences<br>
				{{ Form::radio('ends', 'on') }} On {{ Form::text('endson') }} <br>

			</p>
			</fieldset>
		</div>
        <p>
            {{ Form::hidden('user_id', $user_id) }}

            {{ Form::submit('Save New') }}
        </p>
	{{ Form::close() }}

@stop