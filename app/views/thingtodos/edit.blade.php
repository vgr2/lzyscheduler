@extends('layouts.main')

@section('head')
    @parent
	<!-- <link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" /> -->
	{{ HTML::style('jquery-ui-1.11.0.custom/jquery-ui.css', array('media' => 'all')) }}
    {{ HTML::style('timepicker/jquery-ui-timepicker-addon.css') }}
    
    {{ HTML::script('jquery-ui-1.11.0.custom/jquery-ui.js') }}
	<!-- <script src="{{ URL::asset('datetimepicker/jquery.js') }}"></script> -->
	{{ HTML::script('timepicker/jquery-ui-timepicker-addon.js') }}

	<script type="text/javascript">
	 $(document).ready(function(){
		var startDateTextBox = $('#given');
		var endDateTextBox = $('#due');

		startDateTextBox.datetimepicker({ 
			timeFormat: 'HH:mm:00',
			dateFormat: 'yy-mm-dd',
			onClose: function(dateText, inst) {
				if (endDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						endDateTextBox.datetimepicker('setDate', testStartDate);
				}
				else {
					endDateTextBox.val(dateText);
				}
			},
			onSelect: function (selectedDateTime){
				endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
			}
		});
		endDateTextBox.datetimepicker({ 
			timeFormat: 'HH:mm:00',
			dateFormat: 'yy-mm-dd',
			onClose: function(dateText, inst) {
				if (startDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						startDateTextBox.datetimepicker('setDate', testEndDate);
				}
				else {
					startDateTextBox.val(dateText);
				}
			},
			onSelect: function (selectedDateTime){
				startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
			}
		});
		$('#given').onchange(function () {
			$('#startson').val(this.value);
		});
	});
	</script>    
	<script language="javascript">
		function checkMe() {
		    if (confirm("Are you sure")) {
				document.getElementById("deleteItem").submit();
		        return true;
		    } else {
		        // alert("Clicked Cancel");
		        return false;
		    }
		}
	</script>

@stop

@section('content')

    {{ Form::open(array('url' => 'thingtodos/' . $thingtodo->id, 'class' => 'pull-right', 'id' => 'deleteItem')) }}
	{{ Form::hidden('_method', 'DELETE') }}
	{{ Form::submit('Delete', array('class' => 'btn btn-warning', 'onClick' => 'return checkMe()')) }}
	{{ Form::close() }}
	<div class="h2">
	View / Edit
	</div>

	@foreach ($errors->all() as $error)

		<p class="error">{{ $error }}</p>
	
	@endforeach
	
    <!-- <pre>{{-- print_r($thingtodo) --}}</pre> -->

	{{ Form::model($thingtodo, array('route' => array('thingtodos.update', $thingtodo->id), 'method' => 'PUT' )) }}		
		<p>
			{{ Form::label('title', 'What: ') }}
			{{ Form::text('title') }}
			<br>
			{{ Form::label('location', 'Where: ') }}
			{{ Form::text('location') }}
		</p>
		<p>
			{{ Form::label('allday', 'All day:') }}
			{{-- Form::checkbox('allday') --}}
			{{ $thingtodo->allday }}
			<input type="checkbox" name="allday" value="1" <?php if($thingtodo->allday){ echo 'checked="checked"';} ?>>
			{{ Form::label('recur', 'Repeat:') }}
			{{ Form::checkbox('recur') }} 			
		</p>
		<p>
			{{ Form::label('description', 'Description:') }} <br>
			{{ Form::textarea('description', null, ['rows' =>'3']) }}

		</p>
		<p>
			{{ Form::label('importance', 'Importance: ')}}
			{{ Form::text('importance', null, array(
					'data-slider' => 'true', 
					'data-slider-range' => '1,10',
					'data-slider-step' => '1'
				)) 
			}}
			<br> 
			<small>(on a scale of 1 - 10)</small>
		</p>
		<p>
		<!-- </p>
		<p> -->
			{{ Form::label('given', 'Start:') }}
			{{ Form::text('given') }}
			{{ Form::label('due', 'End:') }}
			{{ Form::text('due') }}
		</p>
		<p>
			{{ Form::label('allottedtime', 'Time Allotted:') }}
			{{ Form::text('allottedtime') }}<br>
			<small>(Expected length of task/event in hours)</small>
		</p>
		<p>
			{{ Form::label('progress', 'Progress')}}: 
			{{ Form::text('progress', null, array('data-slider' => 'true')) }}
		</p>
		<div id="repeats">
			<fieldset>
			<legend><strong>Repeats</strong></legend>
			<p id="every">
				
				Repeat {{ Form::label('every', 'every ') }}

				{{ Form::selectRange('every', 1, 30) }}
			<!-- </p>
			<p class="recurtype"> -->
				{{ Form::select('recurtype', [
					'day' => 'Day',
					'week' => 'Week',
					'month' => 'Monthly',
					'year' => 'Year'
				]) }} period.
				
			</p>
			<p id="weekly">
				{{ Form::label('sun','Su') }}
				{{ Form::checkbox('sun') }}
				{{ Form::label('mon','Mo') }}
				{{ Form::checkbox('mon') }}
				{{ Form::label('tue', 'Tu') }}
				{{ Form::checkbox('tue') }}
				{{ Form::label('wed', 'We') }}
				{{ Form::checkbox('wed') }}
				{{ Form::label('thu', 'Th') }}
				{{ Form::checkbox('thu') }}
				{{ Form::label('fri', 'Fr') }}
				{{ Form::checkbox('fri') }}
				{{ Form::label('sat', 'Sa') }}
				{{ Form::checkbox('sat') }}
			</p>
			<p id="monthly">
				{{ Form::radio('monthlyrepeaton', 'dayofmonth') }} day of month <strong>or</strong> {{ Form::radio('monthlyrepeaton', 'dayofweek') }} day of week
			</p>
			<p id="startson">
				{{ Form::label('startson', 'Starts on: ') }}
				{{ Form::text('startson', '', ['readonly' => 'readonly']) }}
			</p>
			<p id="ends">
				Ends:<br>
				{{ Form::radio('ends', 'never') }} Never <br>
				{{ Form::radio('ends', 'after') }} After {{ Form::text('endsafter') }} occurrences<br>
				{{ Form::radio('ends', 'on') }} On {{ Form::text('endson') }} <br>

			</p>
			</fieldset>
		</div>
        <p>
            {{ Form::hidden('user_id') }}

            {{ Form::submit('Update') }}
        </p>
	{{ Form::close() }}

@stop