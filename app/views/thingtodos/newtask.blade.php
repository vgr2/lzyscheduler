<?php $page = "New Task"; ?>
@extends('layouts.main-bootstrap-fixed')

@section('head')
    @parent
	<!-- <link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" /> -->
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <!--{{-- HTML::script('timepicker/jquery-ui-timepicker-addon.js') --}}-->
	{{-- HTML::script('datetimepicker/jquery.js') --}}
        {{ HTML::style('datetimepicker/jquery.datetimepicker.css') }}
        {{ HTML::script('datetimepicker/jquery.datetimepicker.js') }}
        }

        <style>
            .half-width-float{
                width: 48%; float: left;
            }
        </style>

@stop

@section('content')

	<div class="h2">New Task</div>

	@foreach ($errors->all() as $error)

		<p class="error">{{ $error }}</p>
	
	@endforeach
	
 
	{{ Form::open() }}
		<p>
			{{ Form::label('title', 'What: ') }}
			{{ Form::text('title', '', array('placeholder' => 'Enter something you have to do.', 'class' => 'form-control')) }}
<!--			<br>-->
			{{-- Form::label('location', 'Where: ') --}}
			{{-- Form::text('location', '', array('placeholder' => '', 'class' => 'form-control')) --}}
		</p>
<!--		<p>
			{{ Form::label('allday', 'All day:') }}
			{{ Form::checkbox('allday') }}
			{{ Form::label('recur', 'Repeat:') }}
			{{ Form::checkbox('recur') }} 			
		</p>-->
		<p>
			{{ Form::label('description', 'Description:') }} <br>
			{{ Form::textarea('description', null, array('placeholder' => 'Description of the thing to be done. (optional)', 'rows' =>'3', 'class' => 'form-control')) }}

		</p>
                <p>
                <p style="clear:both;">
                    <div class='half-width-float pull-left'>
                        {{ Form::label('allottedtime', 'Time Needed:') }} <small>(estimate in hours)</small>
                        {{ Form::text('allottedtime', null, array('placeholder' => '', 'class' => 'form-control')) }}
                    </div>
                    <div class='half-width-float pull-right'>
                        {{ Form::label('importance', 'Importance: ') }}
                        <small>(on a scale of 1 - 10)</small>
			{{ Form::text('importance', null, array(
					'data-slider' => 'true', 
					'data-slider-range' => '1,10',
					'data-slider-step' => '1'
				)) 
			}}
                        {{ Form::text('importance1', null, array(
					'type' => 'range', 
					'value' => '50',
					'min' => '0',
                                        'max' => '100'
				)) 
			}}
                        <input name="importance1" type="range" value="50" min="0" max="100" >
                    </div>
		</p>

                
                <p style="clear:both; margin-top: 10px;">
                    <div class='half-width-float pull-left'>
                        {{ Form::label('due', 'Due:') }} <small>(Date and time due)</small>
                        {{ Form::text('due', null, array('placeholder' => 'Click to select date', 'class' => 'form-control')) }}
                    </div>
                    <div class='half-width-float pull-right'>
                        {{ Form::label('progress', 'Progress')}}: 
                        {{ Form::text('progress', null, array('data-slider' => 'true')) }}
                    </div>
                </p>
                <div style="clear:both; margin-top: 10px;">
                    {{ Form::hidden('user_id', $user_id) }}

                    {{ Form::submit('Save New') }}
                </div>
        </p>
	{{ Form::close() }}
    <script>
        $('#due').datetimepicker({
            step: 5,
            format:'Y-m-d H:i',
        });
    </script>
<!--    <script type="text/javascript">
     $(document).ready(function(){
        var timeDue = $('#due');
                
                timeDue.datetimepicker({
                    timeFormat: 'HH:mm:00',
                    stepMinute: 15,
                    dateFormat: 'yy-mm-dd'
                });

    });
    </script>    -->
@stop

