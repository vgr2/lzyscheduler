@extends('layouts.main')

@section('head')
    @parent
	<!-- <link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" /> -->
	{{ HTML::style('jquery-ui-1.11.0.custom/jquery-ui.css', array('media' => 'all')) }}
    {{ HTML::style('timepicker/jquery-ui-timepicker-addon.css') }}
    
    {{ HTML::script('jquery-ui-1.11.0.custom/jquery-ui.js') }}
	<!-- <script src="{{ URL::asset('datetimepicker/jquery.js') }}"></script> -->
	{{ HTML::script('timepicker/jquery-ui-timepicker-addon.js') }}

@stop

@section('content')

	<div class="h2">
	Edit Settings
	</div>

	@foreach ($errors->all() as $error)

		<p class="error">{{ $error }}</p>
	
	@endforeach

	{{ Form::model($setting, array('route' => array('settings.update', $setting->id), 'method' => 'PUT' )) }}		
		<p>
			{{ Form::label('timezone', 'Time Zone: ') }}
			{{ Form::select('timezone', DateTimeZone::listIdentifiers(), $timezone) }}
		</p>
		<p>
			{{ Form::label('extratime', 'Extra Time:') }}
			<input type="checkbox" name="extratime" value="1" <?php if($setting){ echo 'checked="checked"';} ?>>
			{{ Form::label('extratimefactor', 'Extra Time Factor: ') }}
			{{ Form::text('extratimefactor') }}<br>
		</p>
        <p>
            {{ Form::hidden('user_id', $user->id) }}

            {{ Form::submit('Update') }}
        </p>
	{{ Form::close() }}

@stop