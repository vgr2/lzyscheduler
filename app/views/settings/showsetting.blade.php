<?php $page = "Settings"; ?>
@extends('layouts.main-bootstrap-fixed')

@section('head')
    @parent
    
	<script type="text/javascript">
	function update(timezone){

		// x = document.getElementById("timezone").selectedIndex;
		// var timezone = document.getElementById("timezone").options[x].text;

		alert(timezone);

		if (window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {  // code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}

		xmlhttp.onreadystatechange=function() {
		    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		      document.getElementById("select_tz").innerHTML=xmlhttp.responseText;
		    }
		}
		alert("<?= route('update_tz'); ?>?timezone="+timezone);
		xmlhttp.open("GET", "<php? echo route('update_tz'); ?>?timezone="+timezone, true );
		alert(xmlhttp);
		// xmlhttp.send();

	}

	$(document).ready(function() {
	    $('#!tz_btn').click(function() {
	      update($('#timezone').val());
	    });
	});
	</script>    
    
@stop


@section('content')

<div class="maincontent">
	
	@if (Session::has('message'))
		<div class="alert alert-info">{{ Session::pull('message', 'default') }} </div>
	@endif

	<h3>Settings</h3>
	
    <h4>{{ Confide::user()->username }}</h4>
    <div class="well">
        <p><b>E-mail:</b> {{ Confide::user()->email }}</p>
        <hr>
        <p>
        	<strong>Time zone:</strong> <span id="select_tz">{{ $tz_dropdown }}</span>
        </p>


        <!--Add summary of the days activities inclusive of events and tasks-->
        <!--Add statistics charts - e.g. percentage of completed vs incomplete vs overdue tasks-->
    </div>
 </div>

@stop

