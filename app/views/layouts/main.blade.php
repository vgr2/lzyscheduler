<!DOCTYPE html>
<html>
<head>
    
    @section('head')
	<title>LzyFocus Scheduler</title>
	<!-- Include jQuery -->
	<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
	
        {{ HTML::script('js/jquery-2.1.1.js') }}

	<!-- Include Simple Slider JavaScript and CSS -->
	{{ HTML::script('js/simple-slider.js') }}
	{{ HTML::style('css/simple-slider.css') }}

	{{ HTML::style('css/bootstrap.min.css') }}
	{{ HTML::style('css/style.css') }}

	@show

</head>
<body>
<div class="ctnr-width">
	<div class="brand">
            <a class="h1" href="{{ URL::to('/') }}">{{ HTML::image('img/lzyschedulelogorr.png') }}</a>
            <div class="pull-right">
              Work Smarter; Not Harder
            </div>
	</div>
	<nav class="navbar navbar-inverse">
		<ul class="nav navbar-nav">
			<li><a href="{{ URL::to('/') }}">Home</a></li>
			<li><a href="{{ URL::to('/new') }}">New Thing To Do</a></li>
			<li><a href="{{ URL::to('/events') }}">Events</a></li>
			<li><a href="{{ URL::to('/prioritylist') }}">Priority List</a></li>
            <div class="user-opts pull-right">
                @if (Auth::check())
                <?php echo Auth::user()->email; ?>
                <?php /*<a href="{{ URL::to('settings/' . $user_id . '/edit') }}" >
                  <span class="glyphicon glyphicon-cog"></span>
                </a> */ ?> |
                <a href="{{ URL::to('/users/logout') }}">Log out</a>
                @endif
                @if (!Auth::check())
                 <a href="{{ URL::to('/users/create') }}">Register</a> | <a href="{{ URL::to('/users/login') }}">Login</a>
                @endif
            </div>
        </ul>
	</nav>

</div>
<div class="ctnr-width container">
	@yield('content')    
    <hr>
    <div id="!slogan" class="pull-right">by <img src='{{ URL::asset('img/LzyFocusLogorr.png') }}' id='lzyfocuslogo' >  </div>
</div>
</body>
</html>