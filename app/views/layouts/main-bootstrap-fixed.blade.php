<!DOCTYPE html>
<html lang="en">
  <head>
    @section('head')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>LzyFocus Scheduler <?php if (isset($page)){ echo '| '.$page;} else { echo '';} ?></title>

	<!-- Include jQuery -->
	<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
	
        {{ HTML::script('js/jquery-2.1.1.js') }}

	<!-- Include Simple Slider JavaScript and CSS -->
	{{ HTML::script('js/simple-slider.js') }}
	{{ HTML::style('css/simple-slider.css') }}

  {{ HTML::script('js/bootstrap.min.js') }}
  {{ HTML::style('css/bootstrap.min.css') }}
	{{ HTML::style('css/style.css') }}

        {{ HTML::style('css/navbar-fixed-top.css') }}

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        {{ HTML::script('js/ie-emulation-modes-warning.js') }}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    @show
  </head>

  <body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
            <a class="navbar-brand" href="{{ URL::to('/') }}">{{ HTML::image('img/lzyschedulelogorr.png', 'logo', array('style' => 'margin-top: -9px; width: 100%;')) }}</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="{{ pageActive($page, "Home") }}" ><a href="{{ URL::to('/') }}">Home</a></li>
            <li class="dropdown {{ pageActive($page, "Priority List") }}" >
              <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" href="">Things to do <span class="caret"></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL::to('/prioritylist') }}">Priority list</a></li>
                <li><a href="{{ URL::to('/bullseyelist') }}">Bull's eye Mgmt</a></li>
              </ul>
            </li>
            <li class="{{ pageActive($page, "Calendar") }}" ><a href="{{ URL::to('/gcal') }}">Calendar</a></li>
            <!--<li <?php pageActive($page, "New Thing to do"); ?> ><a href="{{ URL::to('/tasks/create') }}">New Task</a></li>-->
            <!-- <li <?php pageActive($page, "Events"); ?> ><a href="{{ URL::to('/events') }}">Events</a></li> -->
            @if (Auth::check())
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  <?php echo Auth::user()->email; ?> <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL::to('/settings') }}">Settings</a></li>
                <li><a href="{{ URL::to('/users/logout') }}">Log out</a></li>
              </ul>
            </li>
                
                <?php /*<a href="{{ URL::to('settings/' . $user_id . '/edit') }}" >
                  <span class="glyphicon glyphicon-cog"></span>
                </a> */ ?>
                
            @endif
            @if (!Auth::check())
            <li><a href="{{ URL::to('/users/create') }}">Register</a></li> 
            <li><a href="{{ URL::to('/users/login') }}">Login</a></li>
            @endif            
          </ul>

        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container-fluid">
        <div class="row-fluid">
          <div class="col-md-1"></div>
          <div class="col-md-10">
            @yield('content')
          </div>
          <div class="col-md-1"></div>
        </div>
        <div class="row" style='margin: 0 auto;'>
          <div class="col-md-2"></div>
            <div class="col-md-8">
              <hr>
              <div class="pull-left"><em><strong>Work Smarter; Not Harder</strong></em></div>
              <div class="pull-right">by <img src='{{ URL::asset('img/LzyFocusLogorr.png') }}' id='lzyfocuslogo' >  </div>
            </div>
          <div class="col-md-2"></div>  
        </div>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>--}}
{{--    {{ HTML::script('js/bootstrap.min.js') }}--}}
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    {{ HTML::script('js/ie10-viewport-bug-workaround.js') }}
  </body>
</html>
<?php 
function pageActive($page, $pgName) {
    if ($page == $pgName){
        return 'active';
    }
}
?>