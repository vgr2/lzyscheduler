<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	
	public function getIndex()
	{

//		$userId = Auth::user()->id;
		$userId = Confide::user()->id;

        $thingstodo = DB::table('thingtodos')->where('user_id', $userId)->get();

        $cal = new CalendarEvent();
        
        $events = [];
        if(isset($thingstodo)){
          foreach ($thingstodo as $t) {
            $avTm = $cal->timeAvail($t->given, $t->due).'<br>';
            if ($t->allottedtime == doubleval($avTm)) {
              $events[] = $t;
            }
          }
        } else { 
        }
        return View::make('events', array(
            'thingtodos' => $events,
            'user_id' => $userId
            )
        );
        
    }
    
    public function getHome(){
        return View::make('home');
    }
        
    public function getDay()
	{

	$userId = Confide::user()->id;

        $thingstodo = DB::table('thingtodos')->where('user_id', $userId)->get();

        $cal = new CalendarEvent();
        $events = [];
        if(isset($thingstodo)){
          foreach ($thingstodo as $t) {
            $avTm = $cal->timeAvail($t->given, $t->due).'<br>';
            if ($t->allottedtime == doubleval($avTm)) {
              $events[] = $t;
            }
          }
        } else { 
        }
        return View::make('day', array(
            'thingtodos' => $events,
            'user_id' => $userId
            )
        );
        
    }

    public function priorityGroup()
	{
        $userId = Confide::user()->id;
        
        $notdue = DB::table('thingtodos')
                ->whereRaw('NOW() <= due' ) // not overdue
                ->where('user_id', '=', $userId) //owner
                ->where('progress', '<', '1')  // incomplete
                ->whereRaw('allottedtime != (TIMESTAMPDIFF(minute,given,due)/60)')
                ->get();
        $overdue = DB::table('thingtodos')
                ->whereRaw('NOW() > due' )
                ->where('user_id', '=', $userId)
                ->where('progress', '<', '1') 
                ->whereRaw('allottedtime != (TIMESTAMPDIFF(minute,given,due)/60)')
                ->get();
        
        $cal = new CalendarEvent();
        $tasks = [];
        if (isset($notdue)){
          foreach ($notdue as $t) {
            $avTm = $cal->timeAvail($t->given, $t->due).'<br>';
            if ($t->allottedtime < doubleval($avTm)) {
              $tasks[] = $t;
            }
          }
        }
        
        $pg = new PriorityList($tasks);
        $pg_overdue = new PriorityList($overdue);

        return View::make('priorityGroup', compact('pg','pg_overdue','notdue','overdue'), array(
            'user_id' => $userId
			));
//        return View::make('priorityGroup', compact('notdue','overdue'), array(
//            'user_id' => $userId
//			));
	}

    public function getBullsEyeTable()
    {
        $data = $this->bullsEyeInput();
        $cols = $data['title_cols'];
        $val = ' 
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th style="text-align: left;">Title</th>';
                        foreach ($cols as $col) {
                            $val .= '<th>'.$col.'</th>';
                        }
                        
                        // <th>'.$cols[1].'</th>
                        // <th>'.$cols[2].'</th>
                        // <th>'.$cols[3].'</th>
                        // <th>'.$cols[4].'</th>
        $val .= '
                    </tr>
                </thead>
                <tbody>';
        foreach ($cols as $col){
            $val .= $this->getBullsEyeTableRows($data['items'], $col, $cols);
        }
        
        $val .= '
                </tbody>
        </table>';
        return $val;
    }

    public function getBullsEyeTableRows($items, $column=null, $cols){
        $rv = '';
        foreach ($items as $key => $item) {
            // echo "<br><b>title</b>: ". $value['title'];
            // echo ", <b>progress</b>: ". $value['progress'];
            
            if ($item['status']){
                // echo "<pre>";
                $stat = (array) $item['status'];
                // var_dump($stat["status"]);
                $item['status'] = $stat["status"];
                // echo "Status: ".$value['status'];
                // echo "<br>Column: ".$column;
                // echo "</pre>";
            }
                
            
            if ($item['progress'] == 1) {
                $item['status'] = 'Complete';
            } elseif ($item['progress'] < 1 && is_null($item['status'])) {
                $item['status'] = "Should do";
            }

            switch ($item['status']) {
                case "Must do now":
                    $rowClass = "success";
                    break;
                case "Can do":
                    $rowClass = "info";
                    break;
                case "Should do":
                    $rowClass = "warning";
                    break;
            }
            
            if ($item['status'] == $column) {
                
                // $rv .= '<div class="row">';
                $rv .= '
                <tr class="tbl_entry '.$rowClass.'">
                ';
                    $rv .= Form::open();
                    $rv .= Form::hidden('task_id', $item['task_id'], ['id' => 'task_id']);
                    $rv .= '
                        <td class="entry_title">';
                            $rv .= $this->taskItem($item['task_id']);
                    $rv .= '
                        </td>
                    ';
                    foreach ($cols as $key => $col) {
                        if ($col == $column) { $check = "checked=\"checked\""; } else {$check = "";}
                        $rv .= '
                        <td class="entry_status" style="text-align:center;">
                            <input onclick="update(\''.$item['task_id'].'\', \''.$col.'\')" id="status'.$item['task_id'].'" name="status'.$item['task_id'].'" type="radio" value="'.$column.'" ' . $check .'>
                        </td>';                        
                    }
                
                $rv .= Form::close().'
                </tr>
                ';
            }
            
        }
        return $rv;
    }

    public function getBullsEyeList(){
        $data = $this->bullsEyeInput();
        $cols = $data['title_cols'];
        $colCount = count($cols);
        $colSz = 12 / $colCount;
        $val = ' 
            <div class="row" style="border: 1px solid #ccc;">';
            foreach ($cols as $col){
                $val .= '<div class="col-sm-'.$colSz.'">'.$col.'</div>';
            }
        $val .= '
            </div>
            <div class="row" style="border: 1px solid #ccc;">';
        foreach ($cols as $col){
            $val .= $this->getBullsEyeColumn($data['items'], $col, $data['radio_cols']);
        }
        $val .= '
            </div>';
        return $val;
    }

    public function getBullsEyeColumn($items, $column=null, $cols){
        $colCount = count($cols);
        $colSz = 12 / $colCount;
        $rv = '';
        $rv .= '<div class="col-sm-'.$colCount.'" style="border: 1px solid blue;">';
        foreach ($items as $key => $item) {

            if ($item['status']){
                $stat = (array) $item['status'];
                $item['status'] = $stat["status"];
            }
                
            
            if ($item['progress'] == 1) {
                $item['status'] = 'Complete';
            } elseif ($item['progress'] < 1 && is_null($item['status'])) {
                $item['status'] = "Can do";
            }
            
            if ($item['status'] == $column) {
                
                $rv .= Form::open();
                $rv .= Form::hidden('task_id', $item['task_id'], ['id' => 'task_id']);
                $rv .= '
                <div class="row">
                <div class="col-sm-12" style="border-top: 1px solid #ccc;">
                    '.$item['title'].' ';
                foreach ($cols as $col){        
                    if ($col == $column) { $check = "checked=\"checked\""; } else {$check = "";}
                    $rv .= '<input onclick="update(\''.$item['task_id'].'\', \''.$col.'\')" title="'.$col.'" id="status" name="status'.$key.'" type="radio" value="'.$column.'" ' . $check .'>';
                }

                $rv .= Form::close().'
                </div> <!-- end col-sm-12 -->
                </div> <!-- end row -->
                ';
            }
            
        }
        $rv .= '</div> 
                ';
        return $rv;
    }

    public function bullsEyeInput()
    {
        $userId = Confide::user()->id;
        $tasks = DB::table('tasks')
            ->select('*')
            ->where('tasks.user_id', '=', $userId)
            ->get();

        foreach ($tasks as $key => $task) {
            $items[$key]['title']       = $task->title;
            $items[$key]['progress']    = $task->progress;
            $items[$key]['due']         = $task->due;
            $items[$key]['status']      = DB::table('statuses')
                                            ->select('status')
                                            ->where('task_id', '=', $task->id)
                                            ->first();
            $items[$key]['task_id']     = $task->id;
        }
        
        // $cols = ['Title', 'Must do now', 'Should do', 'Can do', 'Complete'];
        $title_cols = ['Must do now', 'Can do', 'Should do'];
        $radio_cols = ['Must do now', 'Can do', 'Should do', 'Complete'];
        
        return ['items' => $items, 'title_cols'=>$title_cols, 'radio_cols' => $radio_cols];

    }

    public function bullseye()
    {
        $data = $this->bullsEyeInput();

        $tbl = $this->getBullsEyeList();

        return View::make('bullseye.index', compact('tbl'));
        
    }

    public function bullseyeTable()
    {
        $tbl = $this->getBullsEyeTable();

        return View::make('bullseye.index', compact('tbl'));        
    }
    
    public function updateStatus()
    {

        $data = Input::all();

        // $task = DB::table('tasks')->where('id', '=', $data['task_id'])->first();
        $task = Task::findOrFail($data['task_id']);

        // echo "<pre>";
        // var_dump($task);
        
        if(is_null($task)){
            echo "Task not found";
        } else {
            // $statusObj = DB::table('stat(filename)atuses')->where('task_id', '=', $task->id)->first();
            $statusObj = Status::where('task_id', '=', $task->id)->first();

            
            $validator = Validator::make($data, Status::$rules);


            if ($validator->fails())
            {
                // return Redirect::back()->withErrors($validator)->withInput();
                die("error");
            }

            $tbl_data = $this->bullsEyeInput();

            // echo "<pre>";print_r($tbl_data);die();

            if (isset($statusObj)) {

                $statusObj->update($data);
                
                if ($data['status'] == "Complete") {
                    $task->progress = 1;
                    $task->save();
                }

                echo "Status successfully updated";
                echo $this->getBullsEyeTable();
                // echo $this->getBullsEyeList();

            } else {

                Status::create($data);

                echo "Status successfully added";
                echo $this->getBullsEyeTable();
                // echo $this->getBullsEyeList();
            }   
        }
        
    }
    
    public function priorityGroupNew()
    {        
        $userId = Confide::user()->id;

        $data = Input::get();

        $alltasks = DB::table('tasks')
                ->where('user_id', '=', $userId) //owner
                ->get();
        
        $str = '';
        foreach ($alltasks as $t) {
          $str = $str . ' '.$t->title;
        }
        preg_match_all('|\[(.*?)\]|', $str, $regs) ;
        $tags = $regs[1] ? array_unique(array_map('strtolower', $regs[1])) : array();

        $tag = (isset($data['tag'])) ? '['.$data['tag'].']' : null ;
        
        if ($tag) {
            $notdue = DB::table('tasks')
                    ->whereRaw('NOW() <= due' ) // not overdue
                    ->where('user_id', '=', $userId) //owner
                    ->where('progress', '<', '1') // incomplete
                    ->where('title', 'like', '%'.$tag.'%') 
                    ->get();
            $overdue = DB::table('tasks')
                    ->whereRaw('NOW() > due' )
                    ->where('user_id', '=', $userId)
                    ->where('progress', '<', '1') 
                    ->where('title', 'like', '%'.$tag.'%') 
                    ->get();
            $done = DB::table('tasks')
                    ->where('user_id', '=', $userId)
                    ->where('progress', '>=', '1') 
                    ->orderBy('due','asc')
                    ->where('title', 'like', '%'.$tag.'%') 
                    ->get();
        } else {
            $notdue = DB::table('tasks')
                    ->whereRaw('NOW() <= due' ) // not overdue
                    ->where('user_id', '=', $userId) //owner
                    ->where('progress', '<', '1') // incomplete
                    ->get();
            $overdue = DB::table('tasks')
                    ->whereRaw('NOW() > due' )
                    ->where('user_id', '=', $userId)
                    ->where('progress', '<', '1') 
                    ->get();
            $done = DB::table('tasks')
                    ->where('user_id', '=', $userId)
                    ->where('progress', '>=', '1') 
                    ->orderBy('due','asc')
                    ->get();
        }
        

        
        $complete = [];
        if (isset($done)) {
            foreach ($done as $d) {
                $complete[] = $d;
            }
            $d = null;
        }
        
        // deprecated - auto schedule in calendar
        // $cal = new CalendarEvent();
        $tasks = [];
        if (isset($notdue)){
          foreach ($notdue as $t) {
//            $avTm = $cal->timeAvail($t->given, $t->due).'<br>';
//            if ($t->allottedtime < doubleval($avTm)) {
              $tasks[] = $t;
//            }
          }
          $t = null;
        }
        $late = [];
        if (isset($overdue)) {
            foreach ($overdue as $o) {
                $late[] = $o;
            }
            $o = null;
        }

        // $late = (object) $late;
        
        // echo "<pre>";
        // echo $this->timezone;
        // print_r($notdue);
        // print_r($overdue);
        // print_r($complete);
        $prioritySectors = $this->prioritySectors();
        // $pg = new PriorityGenerator($notdue);
        // $pg_overdue = new PriorityGenerator($late);

        $tasksObj = new Prioritize($tasks, $this->timezone);
        $tasksNotDue = $tasksObj->tasks;

        $overdueTasksObj = new Prioritize($overdue, $this->timezone);
        $tasksOverDue = $overdueTasksObj->tasks;

        // $overdue = new PriorityGenerator($late);
        
        $this->setTaskSectors($tasksNotDue,$prioritySectors);
        // print_r($tasksObj->tasks);die();
        // $tasks = $pg->tasks;
        $this->setTaskSectors($tasksOverDue,$prioritySectors);
        // $overdue = $pg_overdue->tasks;
        // print_r($tasks);
        // print_r($tasks);
        // print_r($overdue);
        // die();

        return View::make('priorityGroup', compact('tasksNotDue','tasksOverDue','complete','tags', 'prioritySectors'), array(
            'user_id' => $userId
            ));
//        return View::make('priorityGroup', compact('notdue','overdue'), array(
//            'user_id' => $userId
//          ));
    }

    public function setTaskSectors($tasks,$prioritySectors)
    {
        // print_r($tasks);die();
        $sectorTasks = [];
        foreach ($prioritySectors as $sector) {
            // echo "<p>===========================<br>";
            // print_r($sector);
            // echo "===========================</p>";
            foreach ($tasks as $task) {
                // echo "<p>--------------------------<br>";
                // print_r($task);
                // echo "--------------------------</p>";
                if ($task->importance > $sector['minImportance'] && $task->importance <= $sector['maxImportance'] && $task->urgency > $sector['minUrgency'] && $task->urgency <= $sector['maxUrgency']) 
                {
                    $task->priorityGrpId = $sector['id'];
                    // $sectorTasks[] = $task;
                }
            }
        }
        // return $sectorTasks;
    }

    public function prioritySectors()
    {
        return $prioritySectors = array(
            $this->prioritySector(1, 'Important / Not Urgent', 5, 10, 0, 0.5),
            $this->prioritySector(2, 'Important / Urgent', 5, 10, 0.5, 1),
            $this->prioritySector(3, 'Not Important / Not Urgent', 0, 5, 0, 0.5),
            $this->prioritySector(4, 'Not Important / Urgent', 0, 5, 0.5, 1)
        );
    }

    public function prioritySector($id, $name, $minImportance, $maxImportance, $minUrgency, $maxUrgency)
    {
        return array(
        'id' => $id,
        'name' => $name,
        'minImportance' => $minImportance,
        'maxImportance' => $maxImportance,
        'minUrgency' => $minUrgency,
        'maxUrgency' => $maxUrgency
        );
    }

    public function completePriorityItems()
    {
        $userId = Confide::user()->id;
//        $thingstodo = Auth::user()->thingstodo;

        $completetasks = DB::table('thingtodos')
            ->whereRaw('NOW() > due' ) // not overdue
            ->where('user_id', '=', $userId) //owner
            ->where('progress', '>=', '1')  // incomplete
            ->whereRaw('allottedtime != (TIMESTAMPDIFF(minute,given,due)/60)')
            ->get();

        $cal = new CalendarEvent();

        $tasks = [];
        if (isset($completetasks)){
            foreach ($completetasks as $t) {
//            $avTm = $cal->timeAvail($t->given, $t->due).'<br>';
//            if ($t->allottedtime < doubleval($avTm)) {
                $tasks[] = $t;
//            }
            }
        }


        $pg = new PriorityGenerator($tasks,$completed=true);

        return View::make('completePriorityItems', compact('pg',''), array(
            'user_id' => $userId
        ));
    }
    public function completePriorityItemsNew()
    {
        $userId = Confide::user()->id;
//        $thingstodo = Auth::user()->thingstodo;

        $completetasks = DB::table('tasks')
            ->whereRaw('NOW() > due' ) // not overdue
            ->where('user_id', '=', $userId) //owner
            ->where('progress', '>=', '1')  // incomplete
//            ->whereRaw('allottedtime != (TIMESTAMPDIFF(minute,given,due)/60)')
            ->get();

        $cal = new CalendarEvent();

        $tasks = [];
        if (isset($completetasks)){
            foreach ($completetasks as $t) {
//            $avTm = $cal->timeAvail($t->given, $t->due).'<br>';
//            if ($t->allottedtime < doubleval($avTm)) {
                $tasks[] = $t;
//            }
            }
        }


        $pg = new PriorityGenerator($tasks,$completed=true);

        return View::make('completePriorityItems', compact('pg',''), array(
            'user_id' => $userId
        ));
    }

    public function googleCal()
    {
        $email = Confide::user()->email;
        // dd($email);
        return View::make('gcal', array('email' => $email));
    }

    public function sendReminderEmail()
    {
        $data = Input::all();
        // dd($data);
        $rules = array(
            'id' => 'required|numeric', 
            'title' => 'required', 
            'due' => 'required', 
            'allottedtime' => 'required|numeric', 
            'progress' => 'required', 
            'user_id' => 'required|numeric', 
            'recipients' => 'email',
            'subject' => 'required', 
            'message' => '', 
            'sendtime' => 'required',

        );

        $validator  = Validator::make ($data, $rules);

        // $sendDate = $data['year'].'-'.$data['month'].'-'.$data['day'].' '.$data['hour'].':'.$data['min'].':00';
        // $nowDate = new DateTime("now");
        
        if ($validator -> passes()) {
            $userEmail = Confide::user()->email;
            echo "success valid, just need to setup email";
            // Mail::send()
            $objDateTime = new DateTime($data['sendtime']);
            $sendTime =  $objDateTime->format(DateTime::RFC2822);
            $params = array(
                'from'           => 'LzyScheduler Notifications <admin@fsi-tech.com>',
                'to'             => '<'.Confide::user()->email.'>',
                'subject'        => $data['subject'],
                'text'           => $data['message'] ,
                'sendtime'       => $data['sendtime']
            );
            
            // $mgClient = new Mailgun('key-67d84b5feb4c6f096ddaae0971aa6728');
            // $domain = "mg.fsi-tech.com";

            # Make the call to the client.
            $result = $mgClient->sendMessage($domain, $data);
            print_r($result);die();
            return Redirect::to('/prioritylist')->with('message', 'Message set');

            // Mail::send('emails.emailreminder', $data, function($message) use ($data)
            // {
            //     //email 'From' field: Get users email add and name
            //     $message->from($data['email'] , $data['first_name']);
            //     //email 'To' field: cahnge this to emails that you want to be notified.                    
            //     $message->to($userEmail, '')->cc($data['recipients'])->subject($data['subject']);

            // });


        } else {
            return Redirect::to('/prioritylist')->withErrors($validator);
        }
        
        
    
    }

    public function generateICS() {
        $data = Input::all();
        $event_parameters = array(
            'uid' =>  $data['uid'], // '123',
            'summary' => $data['summary'], // 'Introduction Quiz Deadline',
            'description' => $data['description'], // 'Make sure you check the website for the latest information',
            'start' => new DateTime($data['start']),
            'end' => new DateTime($data['end']),
            'location' => $data['location']
        );

        $event = new CalendarEvent($event_parameters);

        $calendar_parameters = array (
            'events' => array($event),
            'title' => 'LzyReminder Notifications',
            'author' => 'LzyScheduler Calendars'
        );
        $calendar = new Calendar($calendar_parameters);
        $calendar->generateDownload();
    }

    public function mailGun($data)
    {
        # Include the Autoloader (see "Libraries" for install instructions)
        // require 'vendor/autoload.php';
        // use Mailgun\Mailgun;

        # Instantiate the client.
        $mgClient = new Mailgun('key-67d84b5feb4c6f096ddaae0971aa6728');
        $domain = "mg.fsi-tech.com";

        # Make the call to the client.
        $result = $mgClient->sendMessage($domain, $data);

    }

    public function taskItem($id)
    {
        $todo = Task::findOrFail($id);
        if (isset($task)){
          $timeDue = $task->due;
          $time = $task->allottedtime;
          $days = floor($time/24);
          $hours = floor($time - ($days*24));
          $minutes = floor(($time - ($days*24) - ($hours)) * 60);
        } else {
          $days = 0;
          $hours = 0;
          $minutes = 0;
        }

        $url = 'https://www.google.com/calendar/render?action=TEMPLATE'; // https://www.google.com/calendar/render?action=TEMPLATE&text=[progressum][reproductivesolutionsja]+Go+through+the+content+we+got+from+them&dates=2015-05-19T06:23:48+0000/2015-05-19T06:23:48+0000&details=Go+through+the+content,+sort+and+upload+to+the+form+in+e-dasher&location&pli=1&sf=true&output=xml#main_7
        $url .= '&text='.$todo->title;
        $url .= '&dates='.date('Ymd\THi00\Z').'/'.date('Ymd\THi00\Z');
        $url .= '&details='.$todo->description;
        $url .= '&location=';
        $url .= '&sf=true';
        $url .= '&output=xml';
        //echo $url;

        $rv = '<div class="priorityListItem">';
        $rv .= '<strong><a href="'.URL::route('edit', $todo->id).'" class="itemTitle">'.$todo->title.'</a></strong> ';
        $rv .= '<br><span class="itemMeta">Progress: '.($todo->progress*100).'% ';
        $rv .= ' Due on: '.$todo->due.'. ';
        // $rv .= ' Priority Score: '. $todo->priority;
        $rv .= '</span>';
        $rv .= '<a href="'.$url.'", target="_blank" class="btn btn-primary btn-xs" style="display:inline-block" title="Schedule in Google Calendar">Google <span class="glyphicon glyphicon-calendar"></span></a> ';
        $rv .= ' <div style="display:inline-block">'.' <a href="'.$url.'" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#calendar-reminder-Modal-'.$todo->id.'" title="Schedule in your favorite calendar"><i class="glyphicon glyphicon-time"></i><i class=" glyphicon glyphicon-calendar"></i></a>'.'</div>';
        $rv .= ' <div style="display:inline-block">'.' <a href="#" title="Schedule an email reminder" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#email-reminder-Modal-'.$todo->id.'"><i class=" glyphicon glyphicon-time"></i><i class=" glyphicon glyphicon-envelope"></i></a>'.'</div>';
        $rv .= ' <div style="display:inline-block">';
        $rv .= Form::open(array('route' => array('tasks.update', $todo->id), 'class' => 'form-inline', 'role'=>"form"));
        $rv .= Form::hidden('title', $todo->title);
        $rv .= Form::hidden('importance', $todo->importance);
        $rv .= Form::hidden('due', $todo->due);
        $rv .= Form::hidden('days', $days);
        $rv .= Form::hidden('hours', $hours);
        $rv .= Form::hidden('minutes', $minutes);
        $rv .= Form::hidden('progress', 1);
        $rv .= Form::hidden('user_id', $todo->user_id);
        $rv .= Form::button('<span class="glyphicon glyphicon-ok"></span>', ['class' => 'btn btn-primary btn-xs', 'title' => 'Mark complete', 'type' => 'submit']);
        // $rv .= Form::submit('<span class="glyphicon glyphicon-ok"></span>', ['class' => 'btn btn-primary btn-xs', 'title' => 'Mark complete']);
        $rv .= Form::close();
        $rv .= '</div>';
        $rv .= '
        <div class="modal fade" id="calendar-reminder-Modal-'.$todo->id.'">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Schedule work time in your favourite calendar app</h4>
              </div>
              <div class="modal-body">';
        $rv .= Form::open(array('route' => 'downloadcal', 'class'=>'form', 'id'=>'event-capture-form'));
        $rv .= Form::hidden('uid', $todo->id.date("YmdHis") );
        $rv .= Form::hidden('summary', $todo->title );
        $rv .= Form::hidden('description', $todo->description);

        $rv .= '<div class="form-group">';
        $rv .= Form::label('start', 'Set start date & time:', []);
        $rv .= Form::text('start', null, array('placeholder' => 'Click to select date and time', 'class'=>'form-control', 'id'=>'start-time'.$todo->id));
        $rv .= '</div>';
        $rv .= '<div class="form-group">';
        $rv .= Form::label('end', 'Set end date & time:', []);
        $rv .= Form::text('end', null, array('placeholder' => 'Click to select date and time', 'class'=>'form-control', 'id'=>'end-time'.$todo->id));
        $rv .= '</div>';
        $rv .= '<div class="form-group">';
        $rv .= Form::label('description', 'Description:', []);
        $rv .= Form::textarea('description', $todo->description, array('placeholder' => 'Add a note to the body of message (optional)', 'class'=>'form-control', 'size' => '30x4'));
        $rv .= '</div>';
        $rv .= '<div class="form-group">';
        $rv .= Form::label('location', 'Location:', []);
        $rv .= Form::text('location', null, array('placeholder'=>'Set the event location (optional)', 'class' => 'form-control'));
        $rv .= '</div>';
        $rv .= Form::submit('Set', array('style'=>'display:none', 'id'=>'event-capture-submit-form'));
        $rv .= Form::close();
        $rv .= '</div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <label for="event-capture-submit-form" class="btn btn-primary">Set</label>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->';
        $rv .= '
        <div class="modal fade" id="email-reminder-Modal-'.$todo->id.'">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Select date and time for the email to be sent</h4>
              </div>
              <div class="modal-body">';
                $rv .= Form::open(array('route' => 'emailreminder', 'class'=>'form', 'id'=>'emailreminder'));
                $rv .= Form::hidden('user_id', $todo->user_id );
                $rv .= Form::hidden('id', $todo->id );
                $rv .= Form::hidden('title', $todo->title );
                $rv .= Form::hidden('description', $todo->description);
                $rv .= Form::hidden('due', $todo->due);
                $rv .= Form::hidden('allottedtime', $todo->allottedtime);
                $rv .= Form::hidden('progress', $todo->progress);

                $msg = '';
                // $msg = 'Title: '. $todo->title.'';
                $msg .= (!empty($todo->description)) ? 'Description: '.$todo->description.'; ' : '' ;
                $msg .= '';
                $msg .= 'Progress: '. ($todo->progress*100) .'%; ';
                $msg .= 'Due on: '. $todo->due . '; ';
                $msg .= 'Time allotted: '. $todo->allottedtime .' hour(s); ';

                $rv .= '<div class="form-group">Recipients:'.Form::text('recipients', Auth::user()->email, array('placeholder' => 'Additional recipients (optional) - separate by comma', 'class'=>'form-control')).'</div>';
                $rv .= '<div class="form-group">Subject:'.Form::text('subject', 'LzyReminder: '.$todo->title, array('placeholder' => 'Subject of email', 'class'=>'form-control')).'</div>';
                $rv .= '<div class="form-group">Message:'.Form::textarea('message', $msg, array('placeholder' => 'Add a note to the body of message (optional)', 'class'=>'form-control', 'size' => '30x4')).'</div>';
                // $rv .= '<div class="row">';
                  $rv .= '<div class="form-group">Delivery date and time (Approximate)'.Form::text('sendtime', null, array('placeholder' => 'Click to select date and time', 'class'=>'form-control', 'id'=>'sendtime'.$todo->id)).'</div>';
                  
                $rv .= Form::submit('Set', array('style'=>'display:none', 'id'=>'emailreminder-submit-form'));
                $rv .= Form::close();
              $rv .= '</div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <label for="emailreminder-submit-form" class="btn btn-primary">Set</label>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->';

        $rv .= '</div>';

        $rv .= '<script>';
          $rv .= '$("#start-time'.$todo->id.'").datetimepicker({';
              $rv .= 'step: 15,';
              $rv .= 'format:"Y-m-d H:i",';
              $rv .= 'minDate:"0",';
              $rv .= 'minTime:"'.date("H:i:s").'"';
          $rv .= '});';
          $rv .= '$("#end-time'.$todo->id.'").datetimepicker({';
              $rv .= 'step: 15,';
              $rv .= 'format:"Y-m-d H:i",';
              $rv .= 'minDate:"0",';
              $rv .= 'minTime:"'.date("H:i:s").'"';
          $rv .= '});';
          $rv .= '$("#sendtime'.$todo->id.'").datetimepicker({';
              $rv .= 'step: 5,';
              $rv .= 'format:"Y-m-d H:i:s",';
              $rv .= 'minDate:"0",';
              $rv .= 'minTime:"'.date("H:i:s").'"';
          $rv .= '});';
        $rv .= '</script>';
        return $rv;
    }

}
