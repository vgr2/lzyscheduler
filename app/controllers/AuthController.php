<?php

/**
* 
*/
class AuthController extends Controller
{
	
	public function getLogin()
	{
		return View::make('login');
	}

	public function postLogin()
	{
		$rules = array('username' => 'required', 'password' => 'required');
		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()){
			return Redirect::route('login')->withErrors($validator);
		}

		$auth = Auth::attempt(array(
			'email' => Input::get('username'),
			'password' => Input::get('password')
		), false);

		if (!$auth) {
			return Redirect::route('login')->withErrors(array(
				'Hmmm... Thats not right...'
				));
		}

		return Redirect::route('home');

	}
    
    public function logOut() {
      Auth::logout();
      return Redirect::route('home');
    }

}