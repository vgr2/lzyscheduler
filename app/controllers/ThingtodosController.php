<?php

class ThingtodosController extends \BaseController {

	/**
	 * Display a listing of thingtodos
	 *
	 * @return Response
	 */

	/**
	 * Show the form for creating a new thingtodo
	 *
	 * @return Response
	 */
	public function create()
	{
		$userId = Auth::user()->id;
		return View::make('thingtodos.create', array('user_id' => $userId));
	}

	/**
	 * Store a newly created thingtodo in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Thingtodo::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		// echo "<pre>";print_r($data);die();
		Thingtodo::create($data);

        $cal = new CalendarEvent();
        
        $timeAvail = $cal->timeAvail($data['given'], $data['due']);

        if ($cal->isEvent($data['allottedtime'], $timeAvail)) {
          return Redirect::route('home');
        }
        if ($cal->isTask($data['allottedtime'], $timeAvail)) {
          return Redirect::route('prioritylist');
        }

//		return Redirect::route('home');
	}

	/**
	 * Display the specified thingtodo.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$thingtodo = Thingtodo::findOrFail($id);

		return View::make('thingtodos.show', compact('thingtodo'));
	}

	/**
	 * Show the form for editing the specified thingtodo.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$thingtodo = Thingtodo::find($id);

		return View::make('thingtodos.edit', compact('thingtodo'));
	}

    
	/**
	 * Update the specified thingtodo in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$thingtodo = Thingtodo::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Thingtodo::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$thingtodo->update($data);
        
        $cal = new CalendarEvent();
        
        $timeAvail = $cal->timeAvail($thingtodo->given, $thingtodo->due);
        
//        print_r($thingtodo->allottedtime);
//        print_r($timeAvail);

        if ($cal->isEvent($thingtodo->allottedtime, $timeAvail)) {
          return Redirect::route('home');
        }
        if ($cal->isTask($thingtodo->allottedtime, $timeAvail)) {
          return Redirect::route('prioritylist');
        }

		
	}

	/**
	 * Remove the specified thingtodo from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$thingtodo = Thingtodo::find($id);
		
		Thingtodo::destroy($id);

        $cal = new CalendarEvent();
        
		$timeAvail = $cal->timeAvail($thingtodo->given, $thingtodo->due);

        if ($cal->isEvent($thingtodo->allottedtime, $timeAvail)) {
          return Redirect::route('home');
        }
        if ($cal->isTask($thingtodo->allottedtime, $timeAvail)) {
          return Redirect::route('prioritylist');
        }

		// return Redirect::route('home');
	}

}
