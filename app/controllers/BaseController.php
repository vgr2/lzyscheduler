<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	function __construct()
	{
		if (Auth::check()) {
			$setting = Setting::where('user_id', '=', Confide::user()->id)->firstOrFail();

		 	$this->timezone = $setting->timezone;
			
			setlocale(LC_ALL, $this->timezone);
		}

		// $dailyReminderTime = $setting->dailyReminderTime;

	}

	public function dd($value)
	{
		echo "<pre>";
		print_r($value);
		die();
	}

}
