<?php

class PrioritiesController extends \BaseController {

	/**
	 * Display a listing of priorities
	 *
	 * @return Response
	 */
	public function index()
	{
		$priorities = Priority::all();

		return View::make('priorities.index', compact('priorities'));
	}

	/**
	 * Show the form for creating a new priority
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('priorities.create');
	}

	/**
	 * Store a newly created priority in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Priority::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Priority::create($data);

		return Redirect::route('priorities.index');
	}

	/**
	 * Display the specified priority.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$priority = Priority::findOrFail($id);

		return View::make('priorities.show', compact('priority'));
	}

	/**
	 * Show the form for editing the specified priority.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$priority = Priority::find($id);

		return View::make('priorities.edit', compact('priority'));
	}

	/**
	 * Update the specified priority in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$priority = Priority::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Priority::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$priority->update($data);

		return Redirect::route('priorities.index');
	}

	/**
	 * Remove the specified priority from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Priority::destroy($id);

		return Redirect::route('priorities.index');
	}

}
