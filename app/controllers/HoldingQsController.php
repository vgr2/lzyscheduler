<?php

class HoldingQsController extends \BaseController {

	/**
	 * Display a listing of holdingqs
	 *
	 * @return Response
	 */
	public function index()
	{
		$holdingqs = Holdingq::all();

		return View::make('holdingqs.index', compact('holdingqs'));
	}

	/**
	 * Show the form for creating a new holdingq
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('holdingqs.create');
	}

	/**
	 * Store a newly created holdingq in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Holdingq::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Holdingq::create($data);

		return Redirect::route('holdingqs.index');
	}

	/**
	 * Display the specified holdingq.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$holdingq = Holdingq::findOrFail($id);

		return View::make('holdingqs.show', compact('holdingq'));
	}

	/**
	 * Show the form for editing the specified holdingq.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$holdingq = Holdingq::find($id);

		return View::make('holdingqs.edit', compact('holdingq'));
	}

	/**
	 * Update the specified holdingq in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$holdingq = Holdingq::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Holdingq::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$holdingq->update($data);

		return Redirect::route('holdingqs.index');
	}

	/**
	 * Remove the specified holdingq from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Holdingq::destroy($id);

		return Redirect::route('holdingqs.index');
	}

}
