<?php

class TasksController extends \BaseController {

	/**
	 * Display a listing of tasks
	 *
	 * @return Response
	 */
	public function index()
	{
//		$userId = Confide::user()->id;

//		$tasks = Task::all();

		return Redirect::to('/prioritylist');

		// return View::make('tasks.index', compact('tasks'));
	}

	/**
	 * Show the form for creating a new task
	 *
	 * @return Response
	 */
	public function create()
	{
		$user_id = Confide::user()->id;
		return View::make('tasks.create', compact('user_id'));
	}

	/**
	 * Store a newly created task in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = Input::all();
		// accomodate user feedback re: making 'time needed' unambiguous
		$daysInHours = $data['days']*24;
		$minInHours =  $data['minutes']/60;
		$hours = $data['hours'];
		unset($data['days'],$data['hours'],$data['minutes']); // remove from array, just in case it violates model rules

		// convert back to hours - cuz I dont feel like messing with db structure, migrations, etc.
		$data['allottedtime'] = $daysInHours + $hours + $minInHours;
		$validator = Validator::make($data, Task::$rules, []);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Task::create($data);

		return Redirect::route('tasks.index');
	}

	/**
	 * Display the specified task.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$task = Task::findOrFail($id);

		return View::make('tasks.show', compact('task'));
	}

	/**
	 * Show the form for editing the specified task.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$task = Task::find($id);
        $user_id = Confide::user()->id;

		return View::make('tasks.edit', compact('task', 'user_id'));
	}

	/**
	 * Update the specified task in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$task = Task::findOrFail($id);

        // grab data for time needed and convert to hour for 'allottedtime' field
		$data = Input::all();
		$daysInHours = $data['days']*24;
		$minInHours =  $data['minutes']/60;
		$hours = $data['hours'];
		unset($data['days'],$data['hours'],$data['minutes']);

		// convert back to hours - cuz I don't feel like messing with db structure, migrations, etc.
		$data['allottedtime'] = $daysInHours + $hours + $minInHours; 

		$validator = Validator::make($data, Task::$rules, []);

        if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$task->update($data);

		return Redirect::route('tasks.index');
	}

	/**
	 * Remove the specified task from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Task::destroy($id);

		return Redirect::route('tasks.index');
	}

	public function getDailyReminders()
	{
		
		foreach ($users as $key => $user) {
	    	$tasks = DB::table('tasks')
            ->where('progress', '<', 1) //owner
            ->where('user_id', '=', $user->id)
            ->get();
	 
	        foreach ($tasks as $key => $task) {
	        	//get alarmtime
	        	$date = new DateTime($task->due); // time due
				$date->sub(new DateInterval('PT'.$task->allottedtime.'H')); // less the hours
				echo $date->format('Y-m-d h:i:s') . "\n";

				// assemble message

				// 
	        }
	 
	    }
	}

	public function getUserTasks($user)
	{
		return $tasks = DB::table('tasks')
            ->where('progress', '<', 1) //owner
            ->where('user_id', '=', $user->id)
            ->get();
	}
	public function getDayTasks($tasks)
	{
		foreach ($tasks as $key => $task) {
        	$date = new DateTime($task->due); // time due
			$date->sub(new DateInterval('PT'.$task->allottedtime.'H')); // less the hours
			print_r($date);
			//check if date is = current date
			// if ($date->format('Y-m-d')) {
			// 	# code...
			// }

        }
        // return 
	}

	public function sendDailyReminders()
	{		

	    $users = User::all();
	    foreach ($users as $key => $user) {
	    	$tasks = DB::table('tasks')
	            ->where('progress', '<', 1) //owner
	            ->where('user_id', '=', $user->id)
	            ->get();
	        foreach ($tasks as $key => $task) {
	        	//get alarmtime
	        	$date = new DateTime($task->due); // time due
				$date->sub(new DateInterval('PT'.$task->allottedtime.'H')); // less the hours
				echo $date->format('Y-m-d h:i:s') . "\n";
	        }
	    }
	    // get all task for users
		// $tasks = Tasks::where
		
        $taskList = [];

		// add task to a list if the current date = time due - remaining allotted time
		// repeat for each incomplete task for the user
		// create message body with list and links to schedule each item in the calendar
		// send message to user's email

	}

}
