<?php

class SettingsController extends \BaseController {

	/**
	 * Display the specified setting.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function showSetting()
    {
        $user = Confide::user();
        
        $setting = Setting::where('user_id', '=', $user->id)->firstOrFail();
        
        $tz = $setting->timezone;
        
        // die();
        // dd($setting);
        // echo '<pre>';print_r(serializeArray($setting));die();
        if ( isset($tz) ){
        
            $tz_dropdown = $this->get_tz_dropdown($tz);
        
        } else {
        
            $tz_dropdown = '<div class="alert alert-info">Select your time zone</div>'.$this->get_tz_dropdown();
        
        }

        return View::make('settings.showsetting', compact('tz_dropdown'));
    }

    /**
     * Update the specified setting in storage.
     *
     * @param 
     * @return Response
     */
    public function update_tz()
    {
        $input = Input::all();

        $data['timezone'] = $input['timezone'];
        $data['user_id'] = $input['user_id'];


        // print_r($data);die();

        // $setting = DB::table('settings')->where('user_id', '=', $data['user_id']);
        $setting = Setting::where('user_id', '=', $data['user_id'])->firstOrFail();
        
        $tz = $setting->timezone;
        
        $validator = Validator::make($data, Setting::$rules);

        if ($validator->fails())
        {
            // echo "Error";
            return Redirect::to('mysettings')->withErrors($validator)->withInput();
        } 

        if (empty($tz) && in_array($data['timezone'], DateTimeZone::listIdentifiers())) {

            echo 'create ';
            // dd($data);
            // Setting::create($data);

            $setting = new Setting;

            $setting->timezone = $data['timezone'];

            $setting->user_id = $data['user_id'];

            $setting->save();

            $message = "created";
            // echo $this->get_tz_dropdown($data->timezone);

        } elseif (!empty($tz) && in_array($data['timezone'], DateTimeZone::listIdentifiers())) {

            //update
            echo "update ";
            // dd($data);
            // $setting->update($data);

            $setting = Setting::where('user_id', '=', $data['user_id'])->firstOrFail();

            $setting->timezone = $data['timezone'];

            $setting->save();

            $message = "updated";
            // echo $this->get_tz_dropdown($data->timezone);
        }

        return Redirect::action('SettingsController@showSetting')->with('message', 'Time zone entry '.$message);
    }

    /**
    * generate the form to selet timezone 
    */
    public function get_tz_dropdown($selected_tz = null)
    {
        // dd($selected_tz);
        $rv = Form::open(['route'=>'update_tz', 'id' => 'tzform']);
        $rv .= '
            <select id="timezone" name="timezone" onchange="this.form.submit()">
            ';
                foreach (DateTimeZone::listIdentifiers() as $timezone)
                {
                    if ($selected_tz == $timezone) {
                        $selected = "selected";
                    } else {
                        $selected = "";
                    }
                    $rv .= '<option '. $selected .'>'.$timezone.'</option>';
                }
        $rv .= '
            </select>
        ';
        $rv .= Form::hidden('user_id', Confide::user()->id);
        $rv .= Form::submit('Save', ['id' => 'tz_btn']);
        $rv .= Form::close();
        return $rv;
    }

}
