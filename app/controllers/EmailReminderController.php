<?php  

/**
* 
*/
class EmailReminderController extends BaseController
{
	
	

	public function dailyReminder()
	{
		User::chunk(200, function($users){
			foreach ($users as $key => $user) {
				
				$tasks = $this->getUserTasks($user);
				
				$dayTasks = $this->getDayTasks($tasks);

				// echo "<pre>";print_r($dayTasks);die();
				if (!empty($dayTasks)) {
					# code...
				}
				$taskList = $this->taskList($dayTasks);
				
				$email = $user->email;
				
				$subject = "Your daily reminder from LzyScheduler";
				
				$systemTimezone = new DateTimeZone('America/Jamaica');
				
				// set time to send based on system timezone
				$time = new DateTime('08:00', $systemTimezone);
				
				// get user timezone
				$userSetting = $this->getUserSettings($user->id);

				// $userTz = getUserTz();
				// echo $userTz;

				// echo $userSetting->timezone;
				// die();

				// foreach ($userSetting as $key => $v) {
				// 	echo ($key.": ".$v);
					echo "<hr>";
				// 	if (count($userSetting) > 0 && $key == "timezone") {
				// 		$userTz = $v;
				// 		break;
				// 	}
				// }

				echo "<pre>";var_dump($userSetting->timezone);//die();

				// $userTz = $userSetting->timezone;
				
				// find time in user timezone

				$sendTime = $time->setTimezone(new DateTimeZone($userSetting->timezone));
				
				$this->addToQueue($user->id, $taskList, $email, $subject, $sendTime->format("Y-m-d H:i:s"));


			}
		});
			
	}

	public function getUserTasks($user)
	{
		return $tasks = DB::table('tasks')
            ->where('progress', '<', 1) //owner
            ->where('user_id', '=', $user->id)
            ->get();
	}

	public function getDayTasks($tasks)
	{
		// setlocale(LC_ALL, 'America/Jamaica');
		$daytask = [];
		foreach ($tasks as $key => $task) {
        	$date = new DateTime($task->due); // time due
        	$date->setTimezone(new DateTimeZone('America/Jamaica'));
        	$today = new DateTime(date('Y-m-d'));
        	$today->setTimezone(new DateTimeZone('America/Jamaica'));
        	// echo "Due<br>";
        	// print_r($date);
        	// echo "allottedtime in secs: "; 	
        	$sec = $task->allottedtime * 60*60;
        	// echo "<br>starttime: ";
        	$starttime = $date->sub(new DateInterval("PT".$sec."S")); 
        	// echo $starttime->format('Y-m-d')."<br>";
			
			$diff = $today->diff($starttime);
			// echo "<br>diff betw due and starttime: ";
			// print_r($diff);
			// echo $diff->days;
			//check if alarm date is = current date
			if ($diff->days < 1) {

				$task->starttime = $starttime->format('Y-m-d H:i:s');
				
				$daytask[] = $task;				
			}
			// if ($date->format('Y-m-d')) {
			// 	# code...
			// }
			// echo "<hr>";

        }
        return $daytask;
    }
    
	//assemble messages from user tasks
	public function taskList($dayTasks)
	{
		// echo "<pre>";print_r($dayTasks);die();
		$rv = "";
		foreach ($dayTasks as $key => $task) {
			$rv .= $task->title."<br>";
			$rv .= "Due: ".$task->due.". ";
			$rv .= "Latest recommended start time: ". $task->starttime.".";
		}

		return $rv;
	}

	//add to queue
	public function getUserSettings($user_id)
	{
		return DB::table('settings')->where('user_id', '=', $user_id)->first();

	}

	public function getUserTz()
	{
		$setting = DB::table('settings')->where('user_id', '=', $user_id)->first();
		if ( count($setting) > 0 )
		{
			return $setting->timezone;
		}

		return false;
	}

	public function addToQueue($user_id, $taskList, $email, $subject, $sendTime)
	{
		$data = ['recipient'=>$email, 'subject' => $subject, 'message' => $taskList, 'sendtime'=>$sendTime, 'user_id' => $user_id];

		$queue = new HoldingQ;
		$queue->recipient = $email;
		$queue->subject = $subject;
		$queue->message = $taskList;
		$queue->sendtime = $sendTime;
		$queue->user_id = $user_id;

		if ( count( HoldingQ::where( 'user_id', '=', $user_id )->get() ) < 1 ) {
			// $queue->save();
			echo "<pre>";print_r($queue);die();
		}
		

	}

	public function sendFromQueue()
	{
		// selectall from queue where sendtime 
		// 
	}
}
?>